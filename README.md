# Swim-Infer

Companion code to the publication 'Inferring characteristics of bacterial swimming in biofilm matrix from time-lapse confocal laser scanning microscopy', Ravel et al.

A first notebook (**swimmers.ipynb**) proposes the implementation of swimmer trajectories in a mock biofilm (synthetic ground truth), inference of the ground truth parameter and comparison of the resulting fitted model with the original synthetic ground truth data.

A second notebook (**Plot_From_Swimmer_data.ipynb**) reproduces the figure 2 of the article from the confocal post-processed images.

**How to cite this material**

If you cite the inference method, please cite Ravel et al., 'Inferring characteristics of bacterial swimming in biofilm matrix from time-lapse confocal laser scanning microscopy.' (2022).
If you use the swimmer dataset, please cite 10.5281/zenodo.6560673

## Installation

Get the source code.

```sh
git clone https://forgemia.inra.fr/bioswimmers/swim-infer.git
```

We assume that virtualenv is installed in your python environment. If it is not the case, execute the command in your shell

```sh
pip install virtualenv
```

To install the packages needed for the code execution, we will install a new python virtual environment

```sh
virtualenv -p python3 Bioswimmers
```

Then, activate the virtual environment

```sh
source Bioswimmers/bin/activate
```

and install the required packages


```sh
pip install -r requirements.txt
```

Additional installation is required for latex rendering in the jupyter notebook.

```sh
jupyter nbextension install --py latex_envs --sys-prefix
jupyter nbextension enable latex_envs --py --sys-prefix
```

## Execution

Now, the companion code can be executed. Launch a jupyter notebook

```sh
jupyter notebook 
```

and select the file **swimmers.ipynb** or **Plot_From_Swimmer_data.ipynb**. The whole code can be executed in the notebook.

N.B. : the notebook must be launched from the *Bioswimmers* virtual environment, that must be previously activated with the command

```sh
source Bioswimmers/bin/activate
```
