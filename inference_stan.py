#!python
"""
This file contains all the routines which use the PyStan library. More precisely, these routines are individually associated to STAN models (*.stan file).
Briefly, most of the routines read the stan file, call: StanModel, sampling, check_hmc_diagnostics, and post-process the output data.
All routines are used in the context of Bayesian inference.
The main goal of swimming inference is to recover the model parameters from the tracking data of the swimmers.
The random walk model considered is: Langevin_Interaction_2 (cf RandomWalk2D.py).
The data can either be simulated data or experimental trajectories.
The raw data are positions of swimmers at each time step. However, velocity and acceleration can be computed and will also be referred as swimming data.

Contents:
    - stanSwimmingA:            regression model on the acceleration, to determine the model parameters (no storage of the full matrices)
                                (see swimmerA.stan)
    - plotMap_Obs:              plot the hidden true state z along the observed positions for all swimmers
    - plot_trace:               plot the trace and posterior of a STAN parameter
    - plot_pairs:               plot the pair-plots of the STAN swimming parameters
    - plotMap_CI_oneSwimmer     plot the hidden true state z along the observed positions for all swimmers, with a confidence interval


    - plot_CI:                  plot the hidden true state z along the observed positions regarding one specific swimmer (x or y direction), with a confidence interval
    - plotMap_CI:               
    - plotDataInf:              plot the input data according to the biofilm density
    - plot_parameter:           plot the STAN parameter

    - plot_pairs_batch:         plot the pair-plots of the STAN swimming parameters
    - plot_pairs_batchGlobal:   plot the pair-plots of the STAN swimming parameters
    - computeWAIC:              compute the WAIC indicator for comparing Bayesian models
    - extractYA:                extract the acceleration data batch-wise, from the flobal STAN output
    - computeYA:                compute the reconstructed deterministic and predicted accelerations YA_det and YA_pre, from the input data and swimming parameters
"""

## Load library
import pystan
import pandas as pd
import numpy as np



def stanSwimmingA(Xref, Vref, Aref, truePositionsNb, Nt, TT, Ns, M, N, x_inf, y_inf, dx, dy, dt, yVx, yVy, yB, yGradBx, yGradBy, yAx, yAy, tt_swimmer, swimmer_indexA, swimmer_indexV, swimmer_index, stan_model_file="swimmerA.stan",output_folder='StanInferenceResults'):
    """
    regression model on the acceleration, to determine the model parameters (no storage of the full matrices - CSR-like data structure switched)
    (see swimmerAbis.stan)
    Input:
       - truePositionsNb [int]:    number of swimmer positions to be inferred [<= Ns x TT]
       - Nt [int]:                 size of the position matrix (numberOfSwimmers x numberOfTimeSteps) [TT*Ns]
       - TT [int]:                 number of time steps [Ncycles+1]
       - Ns [int]:                 total number of swimmers
       - M [int]:                  x-index (number of pixels)
       - N [int]:                  y-index (number of pixels)
       - x_inf [float]:            x-origin
       - y_inf [float]:            y-origin
       - dx [float]:               spatial step (x-direction)
       - dy [float]:               spatial step (y-direction)
       - dt [float]:               time step
       - yVx [array]:              data structure which contains the velocity of all swimmers, at each time step (x-direction). Note:   CSR-like vector
       - yVy [array]:              data structure which contains the velocity of all swimmers, at each time step (y-direction). Note:   CSR-like vector
       - yB [array]:               data structure which contains the biofilm density of all swimmers, at each time step. Note:   CSR-like vector
       - yGradBx [array]:          data structure which contains the gradient of the biofilm density of all swimmers, at each time step (x-direction). Note:   CSR-like vector
       - yGradBy [array]:          data structure which contains the gradient of the biofilm density of all swimmers, at each time step (y-direction). Note:   CSR-like vector
       - yAx [array]:              inference data: structure which contains the accelerations of all swimmers, at each time step (x-direction). Note:   CSR-like vector
       - yAy [array]:              inference data: structure which contains the accelerations of all swimmers, at each time step (y-direction). Note:   CSR-like vector
       - tt_swimmer [array]:       time data. Note:   CSR-like structure
       - swimmer_indexA [array]:   data structure which contains the index of the first information in swimmers accelerations y for a given swimmer. Note:   CSR-like structure
       - swimmer_indexV [array]:   data structure which contains the index of the first information in swimmers velocities y for a given swimmer. Note:   CSR-like structure
       - swimmer_index [array]:    data structure which contains the index of the first information in swimmers positions y for a given swimmer. Note:   CSR-like structure
       - stan_model_file [str]:    name of the STAN file containing the STAN model, optional
       - output_folder [str]       output folder.
    Output:
       - fit []:                   StanModel output
       - df [DataFrame]:           summary DataFrame
    """

    import gzip, pickle
    with open(stan_model_file,'r') as f:
        model=f.read()

    # Put our data in a dictionary
    dataStan = {'Xref':Xref, 'Vref':Vref, 'Aref':Aref, 'zNb':truePositionsNb, 'NSwimObs':Nt, 'TT':TT, 'Ns':Ns, 'M':M, 'N':N, 'x_inf':x_inf, 'y_inf':y_inf, 'dx':dx, 'dy':dy, 'dt':dt, 'yVx':yVx, 'yVy':yVy, 'yB':yB, 'yGradBx':yGradBx, 'yGradBy':yGradBy, 'yAx':yAx, 'yAy':yAy, 'tt_swimmer':tt_swimmer, 'swimmer_indexA':swimmer_indexA, 'swimmer_indexV':swimmer_indexV, 'swimmer_index':swimmer_index}

    # Compile the model
    extra_compile_args = ['-pthread', '-DSTAN_THREADS','-w']
    print('Compiling')
    sm = pystan.StanModel(model_code=model, extra_compile_args=extra_compile_args)
    print('End compiling')
    # Train the model and generate samples
    print("Starting inference")
    fit = sm.sampling(data=dataStan, iter=2000, chains=4, n_jobs=4)#, control=dict(max_treedepth=12))#, adapt_delta=0.9))
    print(fit)
    check = pystan.check_hmc_diagnostics(fit)


    summary_dict = fit.summary()
    df = pd.DataFrame(summary_dict['summary'],
                  columns=summary_dict['summary_colnames'],
                  index=summary_dict['summary_rownames'])

    with gzip.open(output_folder+'/STAN_Abis_fit.dat', 'wb') as fp:
        pickle.dump({'model':sm, 'fit':fit},fp)
    with gzip.open(output_folder+'/STAN_Abis_sigma0.dat', 'wb') as fp:
        pickle.dump(fit['sigma0'],fp)
    with gzip.open(output_folder+'/STAN_Abis_v0.dat', 'wb') as fp:
        pickle.dump(fit['v0'],fp)
    with gzip.open(output_folder+'/STAN_Abis_v1.dat', 'wb') as fp:
        pickle.dump(fit['v1'],fp)
    with gzip.open(output_folder+'/STAN_Abis_gamma.dat', 'wb') as fp:
        pickle.dump(fit['gamma'],fp)
    with gzip.open(output_folder+'/STAN_Abis_beta.dat', 'wb') as fp:
        pickle.dump(fit['beta'],fp)

    return fit,df



def plotMap_Obs(tag,ax,k,NswimmersTot,zx,zy,Nswimmers,swimmer_index,length_x_inf,length_x_sup,length_y_inf,length_y_sup):
    """
    plot the hidden true state z along the observed positions for all swimmers
    Input:
       - yx [array]:               observed positions (x-direction). Note:   CSR-like vector
       - yy [array]:               observed positions (y-direction). Note:   CSR-like vector
       - zx [array]:               hidden true state (x-direction). Note:   CSR-like vector
       - zy [array]:               hidden true state (y-direction). Note:   CSR-like vector
       - Nswimmers [int]:          total number of swimmers
       - swimmer_index [array]:    data structure which contains the index of the first information in swimmers positions y for a given time step. Note:   CSR-like structure
       - length_x_inf [float]:    x-origin
       - length_x_sup [float]:    x-boundary
       - length_y_inf [float]:     y-origin
       - length_y_sup [float]:    y-boundary
    Output:
       void
    """

    import matplotlib.pyplot as plt
    for s in range(Nswimmers):
        it = swimmer_index[s]-1
        itp1 = swimmer_index[s+1]-1
        if (tag==0):
            ax.plot(zy[it:itp1],zx[it:itp1],'o-',linewidth=1.5, c=plt.cm.Blues(128+int(128*(s+k)/NswimmersTot)),marker='o',markersize=1, 
markevery=1000000000,markerfacecolor=plt.cm.Greens(int(255*(s+k)/NswimmersTot)),alpha=1)
        elif (tag==1):
            ax.plot(zy[it:itp1],zx[it:itp1],'o-',linewidth=1.5, c=plt.cm.Reds(128+int(128*(s+k)/NswimmersTot)),marker='o',markersize=1, 
markevery=1000000000,markerfacecolor=plt.cm.Greens(int(255*(s+k)/NswimmersTot)),alpha=1)
        else:
            ax.plot(zy[it:itp1],zx[it:itp1],'o-',linewidth=1.5, c=plt.cm.Greens(128+int(128*(s+k)/NswimmersTot)),marker='o',markersize=1, 
markevery=1000000000,markerfacecolor=plt.cm.Greens(int(255*(s+k)/NswimmersTot)),alpha=1)
    ax.set_ylim(length_x_inf, length_x_sup)
    ax.set_xlim(length_y_inf, length_y_sup)
    ax.set(xticks=[],yticks=[])
    plt.axis("square")
    plt.gcf().tight_layout()



def plot_trace(param, label):
    """
    plot the trace and posterior of a STAN parameter
    Input:
       - param [vector]:                    STAN parameter
       - label [str]:                       name of the parameter
    Output:
       void
    """

    import matplotlib.pyplot as plt
    import seaborn as sns
    numberOfBins = 100

    # Summary statistics
    mean = np.mean(param)
    median = np.median(param)
    cred_min, cred_max = np.percentile(param, 5), np.percentile(param, 95)

    #Plotting
    plt.figure()
    plt.subplot(2,1,1)
    plt.plot(param)
    plt.xlabel('samples')
    plt.ylabel(label)
    plt.axhline(mean, color='r', lw=2, linestyle='--')
    plt.axhline(median, color='c', lw=2, linestyle='--')
    plt.axhline(cred_min, linestyle=':', color='k', alpha=0.2)
    plt.axhline(cred_max, linestyle=':', color='k', alpha=0.2)
    plt.title('Trace and Posterior Distribution for {}'.format(label))
    plt.subplot(2,1,2)
    plt.hist(param, numberOfBins, density=True); sns.kdeplot(param, shade=True)
    plt.xlabel(label)
    plt.ylabel('density')
    plt.axvline(mean, color='r', lw=2, linestyle='--',label='mean')
    plt.axvline(median, color='c', lw=2, linestyle='--',label='median')
    plt.axvline(cred_min, linestyle=':', color='k', alpha=0.2, label='95% CI')
    plt.axvline(cred_max, linestyle=':', color='k', alpha=0.2)
    plt.gcf().tight_layout()
    plt.legend()
    plt.savefig('../Article/IMAGES/traceplot{}.pdf'.format(label),format='pdf')



def plot_pairs(sigma,v0,v1,gamma,beta):
    """
    plot the pair-plots of the STAN swimming parameters
    Input:
       - sigma [vect]:                                 standard deviation of the global noise
       - v0 [vect]:                                    minimal velocity in the biofilm
       - v1 [vect]:                                    maximal velocity in the biofilm
       - gamma [vect]:                                 penalisation coefficient
       - beta [vect]:                                  mean acceleration due to the gradient term
    Output:
       void
    """
    import matplotlib.pyplot as plt
    from matplotlib.ticker import FormatStrFormatter
    axes = pd.plotting.scatter_matrix(pd.DataFrame(np.column_stack((sigma,v0,v1,gamma,beta)), columns=[r'$\epsilon$',r'$v_0$',r'$v_1$',r'$\gamma$',r'$\beta$']), alpha=0.2, hist_kwds={'bins':100})
    n = 5
    for x in range(n):
        for y in range(n):
            ax = axes[x, y]
            ax.yaxis.set_major_formatter(FormatStrFormatter('%.2f'))
            ax.xaxis.set_major_formatter(FormatStrFormatter('%.2f'))
    plt.tight_layout()
    plt.savefig('../Article/IMAGES/pairplot.pdf',format='pdf')





def plotMap_CI_oneSwimmer(s,yx,yy,zx,zy,zCIlx,zCIux,zCIly,zCIuy,Nswimmers,swimmer_index,length_x_inf,length_x_sup,length_y_inf,length_y_sup):
    """
    plot the hidden true state z along the observed positions for one specific swimmers, with a confidence interval
    Input:
       - s [int]:                  index of the desired swimmer to plot
       - yx [array]:               observed positions (x-direction). Note:   CSR-like vector
       - yy [array]:               observed positions (y-direction). Note:   CSR-like vector
       - zx [array]:               hidden true state (x-direction). Note:   CSR-like vector
       - zy [array]:               hidden true state (y-direction). Note:   CSR-like vector
       - zCIlx [array]:            lower boundary of the confidence interval of the hidden true state (x-direction). Note:   CSR-like structure
       - zCIux [array]:            upper boundary of the confidence interval of the hidden true state (x-direction). Note:   CSR-like structure
       - zCIly [array]:            lower boundary of the confidence interval of the hidden true state (y-direction). Note:   CSR-like structure
       - zCIuy [array]:            upper boundary of the confidence interval of the hidden true state (y-direction). Note:   CSR-like structure
       - Nswimmers [int]:          total number of swimmers
       - swimmer_index [array]:    data structure which contains the index of the first information in swimmers positions y for a given time step. Note:   CSR-like structure
       - length_x_inf [float]:    x-origin
       - length_x_sup [float]:    x-boundary
       - length_y_inf [float]:     y-origin
       - length_y_sup [float]:    y-boundary
    Output:
       void    
    """


    import matplotlib.pyplot as plt
    fig, ax = plt.subplots()
    it = swimmer_index[s]-1
    itp1 = swimmer_index[s+1]-1
    ax.plot(yy[it:itp1],yx[it:itp1],'+-',linewidth=1, c='red',marker='o',markersize=0.5, 
markevery=1000000000,markerfacecolor='red', alpha=1, zorder=10) 
    plt.fill(np.append(zCIly[it:itp1], zCIuy[it:itp1][::-1]), np.append(zCIlx[it:itp1], zCIux[it:itp1][::-1]), color='k', alpha=0.5, lw=0.5)
    plt.axis("square")
    plt.gcf().tight_layout()
    plt.show()







def computeYAnobatch_v4(Xref, Vref, Aref, zNb, Ns, dt, yVx, yVy, yB, yGradBx, yGradBy, yAx, yAy, swimmer_indexA, swimmer_indexV, swimmer_index, gamma, v0, v1, beta, sigma):
    """
    Compute the reconstructed deterministic and predicted accelerations YA_det and YA_pre, from the input data and swimming parameters
    Input:
       - Xref [real]:              length of reference
       - Vref [real]:              velocity of reference
       - Aref [real]:              acceleration of reference
       - zNb [int]:                number of swimmer positions to be inferred [<= Ns x TT]
       - Ns [int]:                 total number of swimmers
       - dt [float]:               time step
       - yVx [array]:              data structure which contains the velocity of all swimmers, at each time step (x-direction). Note:   CSR-like vector
       - yVy [array]:              data structure which contains the velocity of all swimmers, at each time step (y-direction). Note:   CSR-like vector
       - yB [array]:               data structure which contains the biofilm density of all swimmers, at each time step. Note:   CSR-like vector
       - yGradBx [array]:          data structure which contains the gradient of the biofilm density of all swimmers, at each time step (x-direction). Note:   CSR-like vector
       - yGradBy [array]:          data structure which contains the gradient of the biofilm density of all swimmers, at each time step (y-direction). Note:   CSR-like vector
       - yAx [array]:              inference data: structure which contains the accelerations of all swimmers, at each time step (x-direction). Note:   CSR-like vector
       - yAy [array]:              inference data: structure which contains the accelerations of all swimmers, at each time step (y-direction). Note:   CSR-like vector
       - swimmer_indexA [array]:   data structure which contains the index of the first information in swimmers accelerations y for a given swimmer. Note:   CSR-like structure
       - swimmer_indexV [array]:   data structure which contains the index of the first information in swimmers velocities y for a given swimmer. Note:   CSR-like structure
       - swimmer_index [array]:    data structure which contains the index of the first information in swimmers positions y for a given swimmer. Note:   CSR-like structure
       - gamma [array]:
       - v0 [array]:
       - v1 [array]:
       - beta [array]:
       - sigma [array]:
    Output:
       - Rsquare [array]:                              R square
       - YAx_det [array]:                              reconstructed deterministic acceleration (x-direction)
       - YAy_det [array]:                              reconstructed deterministic acceleration (x-direction)
       - YAx_pre [array]:                              predicted acceleration output (x-direction)
       - YAy_pre [array]:                              predicted acceleration output (y-direction)
    """

    YAx_pre = np.zeros((1, zNb-2*Ns))
    YAy_pre = np.zeros((1, zNb-2*Ns))
    YAx_det = np.zeros((1, zNb-2*Ns))
    YAy_det = np.zeros((1, zNb-2*Ns))
    myzero = 1e-12
    RsquareDet = np.zeros((1))
    Rsquare = np.zeros((1))
    RsquareTmp = np.zeros((1))
    RsquareTmpbis = np.zeros((1))
    gammaM = np.mean(gamma) 
    v0M = np.mean(v0) 
    v1M = np.mean(v1)
    betaM = np.mean(beta)
    sigmaM = np.mean(sigma)

    for k in range(1):
        count = 0
        for s in range(Ns):
            eps = sigmaM*np.random.multivariate_normal([0,0], np.eye(2),224)
            if (s<Ns):
                it0Y = swimmer_index[s]-1
                it0V = swimmer_indexV[s]-1
                it0 = swimmer_indexA[s]-1
                it0p1 = swimmer_indexA[s+1]-1

            for t in np.arange(it0,it0p1):
                if (s<Ns):
                    V = np.sqrt(yVx[it0V+t-it0]**2 + yVy[it0V+t-it0]**2)
                    gbx = yGradBx[it0Y+t-it0+1]
                    gby = yGradBy[it0Y+t-it0+1]
                G = np.sqrt(gbx**2 + gby**2)

                if (s<Ns):
                    YAx_det[k,t] = gammaM*(v0M + yB[it0Y+t-it0+1]*(v1M-v0M) - V)*yVx[it0V+t-it0]/(V+myzero) + betaM*gbx/(G+myzero)
                    YAx_pre[k,t] = YAx_det[k,t] + eps[t-it0,0]
                    count += 1
                    RsquareDet[k] += (yAx[t] - YAx_det[k,t])**2
                    Rsquare[k] += (yAx[t] - YAx_pre[k,t])**2
                    RsquareTmp[k] += (np.abs(yAx[t] - YAx_det[k,t]) - eps[t-it0,0])**2
                    RsquareTmpbis[k] += (yAx[t] - YAx_det[k,t])**2

                    YAy_det[k,t] = gammaM*(v0M + yB[it0Y+t-it0+1]*(v1M-v0M) - V)*yVy[it0V+t-it0]/(V+myzero) + betaM*gby/(G+myzero)
                    YAy_pre[k,t] = YAy_det[k,t] + eps[t-it0,1]
                    count += 1
                    RsquareDet[k] += (yAy[t] - YAy_det[k,t])**2
                    Rsquare[k] += (yAy[t] - YAy_pre[k,t])**2
                    RsquareTmp[k] += (np.abs(yAy[t] - YAy_det[k,t]) - eps[t-it0,1])**2
                    RsquareTmpbis[k] += (yAy[t] - YAy_det[k,t])**2

        RsquareDet[k] = 1 - RsquareDet[k]/(count*np.var(np.concatenate((yAx,yAy),axis=0)))
        Rsquare[k] = 1 - Rsquare[k]/(count*np.var(np.concatenate((yAx,yAy),axis=0)))
        RsquareTmp[k] = RsquareTmp[k]/RsquareTmpbis[k]/2
        print("*** INFERENCE STAT ***")
        print("COUNT     :",count==2*(zNb-2*Ns), RsquareTmp[k],RsquareTmpbis[k],count*sigmaM*sigmaM,sigmaM,np.abs(np.var(np.concatenate((yAx-YAx_det[0,:],yAy-YAy_det[0,:]),axis=0))-sigmaM*sigmaM),np.mean(np.concatenate((yAx-YAx_det[0,:],yAy-YAy_det[0,:]),axis=0)))
        print("COUNT     :",count)
        print("Var       :",RsquareTmpbis[k],count*sigmaM*sigmaM)
        print("VarDIFF   :",np.abs(RsquareTmpbis[k]/count-sigmaM*sigmaM),np.abs(np.var(np.concatenate((yAx-YAx_det[0,:],yAy-YAy_det[0,:]),axis=0))-sigmaM*sigmaM))
        print("MOY       :",np.mean(np.concatenate((yAx-YAx_det[0,:],yAy-YAy_det[0,:]),axis=0)))
        print("Rsquare   :",Rsquare)
        print("RsquareDet:",RsquareDet)
        print("**********************")

    return RsquareDet, Rsquare, YAx_pre,YAy_pre,YAx_det,YAy_det






def computeLambdaYAnobatch_v4(Xref, Vref, Aref, zNb, Ns, dt, yVx, yVy, yB, yGradBx, yGradBy, yAx, yAy, swimmer_indexA, swimmer_indexV, swimmer_index, gamma, v0, v1, beta, sigma):
    """
    Compute the relative contribution of each term in the swimmer motion equation
    Input:
       - Xref [real]:              length of reference
       - Vref [real]:              velocity of reference
       - Aref [real]:              acceleration of reference
       - zNb [int]:                number of swimmer positions to be inferred [<= Ns x TT]
       - Ns [int]:                 total number of swimmers
       - dt [float]:               time step
       - yVx [array]:              data structure which contains the velocity of all swimmers, at each time step (x-direction). Note:   CSR-like vector
       - yVy [array]:              data structure which contains the velocity of all swimmers, at each time step (y-direction). Note:   CSR-like vector
       - yB [array]:               data structure which contains the biofilm density of all swimmers, at each time step. Note:   CSR-like vector
       - yGradBx [array]:          data structure which contains the gradient of the biofilm density of all swimmers, at each time step (x-direction). Note:   CSR-like vector
       - yGradBy [array]:          data structure which contains the gradient of the biofilm density of all swimmers, at each time step (y-direction). Note:   CSR-like vector
       - yAx [array]:              inference data: structure which contains the accelerations of all swimmers, at each time step (x-direction). Note:   CSR-like vector
       - yAy [array]:              inference data: structure which contains the accelerations of all swimmers, at each time step (y-direction). Note:   CSR-like vector
       - swimmer_indexA [array]:   data structure which contains the index of the first information in swimmers accelerations y for a given swimmer. Note:   CSR-like structure
       - swimmer_indexV [array]:   data structure which contains the index of the first information in swimmers velocities y for a given swimmer. Note:   CSR-like structure
       - swimmer_index [array]:    data structure which contains the index of the first information in swimmers positions y for a given swimmer. Note:   CSR-like structure
       - gamma [array]:            equation parameter
       - v0 [array]:               equation parameter
       - v1 [array]:               equation parameter
       - beta [array]:             equation parameter
       - sigma [array]:            equation parameter
    Output:
       - lambdaYA_1 [array]:       relative contribution of speed selection
       - lambdaYA_2 [array]:       relative contribution of direction selection
       - lambdaYA_3 [array]:       relative contribution of the random term
    """
    lambdaYAx_1 = np.zeros((zNb-2*Ns))
    lambdaYAx_2 = np.zeros((zNb-2*Ns))
    lambdaYAx_3 = np.ones((zNb-2*Ns))
    lambdaYAy_1 = np.zeros((zNb-2*Ns))
    lambdaYAy_2 = np.zeros((zNb-2*Ns))
    lambdaYAy_3 = np.ones((zNb-2*Ns))
    lambdaYA_1 = np.zeros((zNb-2*Ns))
    lambdaYA_2 = np.zeros((zNb-2*Ns))
    lambdaYA_3 = np.ones((zNb-2*Ns))
    myzero = 1e-12
    gammaM = np.mean(gamma) 
    v0M = np.mean(v0) 
    v1M = np.mean(v1) 
    betaM = np.mean(beta) 
    sigmaM = np.mean(sigma) 

    for k in range(1):
        for s in range(Ns):
            eps = sigmaM*np.random.multivariate_normal([0,0], np.eye(2),224)
            if (s<Ns):
                it0Y = swimmer_index[s]-1
                it0V = swimmer_indexV[s]-1
                it0 = swimmer_indexA[s]-1
                it0p1 = swimmer_indexA[s+1]-1

            for t in np.arange(it0,it0p1):
                if (s<Ns):
                    V = np.sqrt(yVx[it0V+t-it0]**2 + yVy[it0V+t-it0]**2)
                    gbx = yGradBx[it0Y+t-it0+1]
                    gby = yGradBy[it0Y+t-it0+1]
                G = np.sqrt(gbx**2 + gby**2)

                if (s<Ns):
                    lambdaYAx_1[t] = gammaM*(v0M + yB[it0Y+t-it0+1]*(v1M-v0M) - V)*yVx[it0V+t-it0]/(V+myzero) #)/(yAx[t]+myzero)
                    lambdaYAx_2[t] = betaM*gbx/(G+myzero) #)/(yAx[t]+myzero)
                    lambdaYAx_3[t] = yAx[t] - lambdaYAx_1[t] - lambdaYAx_2[t]

                    lambdaYAy_1[t] = gammaM*(v0M + yB[it0Y+t-it0+1]*(v1M-v0M) - V)*yVy[it0V+t-it0]/(V+myzero) #)/(yAy[t]+myzero)
                    lambdaYAy_2[t] = betaM*gby/(G+myzero) #)/(yAy[t]+myzero)
                    lambdaYAy_3[t] = yAy[t] - lambdaYAy_1[t] - lambdaYAy_2[t]

                    lambdaYA_1[t] = gammaM*np.abs(v0M + yB[it0Y+t-it0+1]*(v1M-v0M) - V)
                    lambdaYA_2[t] = betaM
                    lambdaYA_3[t] = np.sqrt((yAx[t] - lambdaYAx_1[t] - lambdaYAx_2[t])**2 + (yAy[t] - lambdaYAy_1[t] - lambdaYAy_2[t])**2)

                    lambdaTot = lambdaYA_1[t] + lambdaYA_2[t] + lambdaYA_3[t]
                    lambdaYA_1[t] = lambdaYA_1[t]/lambdaTot
                    lambdaYA_2[t] = lambdaYA_2[t]/lambdaTot
                    lambdaYA_3[t] = lambdaYA_3[t]/lambdaTot

    return lambdaYA_1, lambdaYA_2, lambdaYA_3


def convert(dict_data, control=False):
    '''
    Data structure conversion.

    Input:
        - dict_data:               dictionnary of data that should contain
        Mandatory data (for data in biofilm or in control data without biofilm)
       - yVx [array]:              data structure which contains the velocity of all swimmers, at each time step (x-direction). Note:   CSR-like vector
       - yVy [array]:              data structure which contains the velocity of all swimmers, at each time step (y-direction). Note:   CSR-like vector
       - zNb [int]:                number of swimmer positions to be inferred [<= Ns x TT]
       - Ns [int]:                 total number of swimmers
       - swimmer_indexA [array]:   data structure which contains the index of the first information in swimmers accelerations y for a given swimmer. Note:   CSR-like structure
       - swimmer_indexV [array]:   data structure which contains the index of the first information in swimmers velocities y for a given swimmer. Note:   CSR-like structure
       - swimmer_index [array]:    data structure which contains the index of the first information in swimmers positions y for a given swimmer. Note:   CSR-like structure
        Accessory data (if data in biofilm)
       - yB [array]:               data structure which contains the biofilm density of all swimmers, at each time step. Note:   CSR-like vector
       - yGradBx [array]:          data structure which contains the gradient of the biofilm density of all swimmers, at each time step (x-direction). Note:   CSR-like vector
       - yGradBy [array]:          data structure which contains the gradient of the biofilm density of all swimmers, at each time step (y-direction). Note:   CSR-like vector
       
    Output:
       - yBa [array]:              data structure which contains the biofilm density of all swimmers, at each time step.
       - yGradBxa [array]:         data structure which contains the gradient of the biofilm density of all swimmers, at each time step (x-direction).
       - yGradBya [array]:         data structure which contains the gradient of the biofilm density of all swimmers, at each time step (y-direction).
       - yVxa [array]:             data structure which contains the velocity of all swimmers, at each time step (x-direction).
       - yVya [array]:             data structure which contains the velocity of all swimmers, at each time step (y-direction).
    '''
     
    swimmer_index = dict_data['YindexFirst']
    swimmer_indexA = dict_data['YindexFirstA']
    swimmer_indexV = dict_data['YindexFirstV']    
    zNb = dict_data['YindexFirst'][-1]-1
    Ns = dict_data['data'].swimmer_Nb
    YVxa = np.zeros((zNb-2*Ns))
    YVya = np.zeros((zNb-2*Ns))
    if control==False:
        YBa = np.zeros((zNb-2*Ns))
        YgradBxa = np.zeros((zNb-2*Ns))
        YgradBya = np.zeros((zNb-2*Ns))


    for s in range(Ns):
        it0Y = swimmer_index[s]-1
        it0V = swimmer_indexV[s]-1
        it0Vp1 = swimmer_indexV[s+1]-1
        it0 = swimmer_indexA[s]-1
        it0p1 = swimmer_indexA[s+1]-1

        for t in np.arange(it0,it0p1):
            YVxa[t] = dict_data['YVx'][it0V+t-it0]
            YVya[t] = dict_data['YVy'][it0V+t-it0]
            if control==False:        
                YBa[t] = dict_data['YB'][it0Y+t-it0+1]
                YgradBxa[t] = dict_data['YgradBx'][it0Y+t-it0+1]
                YgradBya[t] = dict_data['YgradBy'][it0Y+t-it0+1]
    if control==False:    
        return YBa, YgradBxa, YgradBya, YVxa, YVya
    else:
        return YVxa, YVya
