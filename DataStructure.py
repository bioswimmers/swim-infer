import numpy as np


class Data(object):
    """
            compute the swimming data outputs including data descriptors from the whole set of swimmer trajectories
    Contains:
        variables:
            - swimmer_Nb [int]:                total number of swimmers
            - dt [float]:                    time step
            - Tmax [float]:                    final time of all trajectories
            - length_x_inf [float]:            x-origin
            - length_x_sup [float]:            x-boundary
            - length_y_inf [float]:            y-origin
            - length_y_sup [float]:            y-boundary
            - dx [float]:                    spatial step (x-direction)
            - dy [float]:                    spatial step (y-direction)
            - m [int]:                        x-index (number of pixels)
            - n [int]:                        y-index (number of pixels)
            - b [array]:                    biofilm density (image stack stored as [Ncycle x m x n])
            - gradbX [array]:                gradient of the biofilm density (x-direction - [Ncycle x m x n])
            - gradbY [array]:                gradient of the biofilm density (y-direction - [Ncycle x m x n])
            - v0 [float]:                    minimal velocity in the biofilm [model parameter]
            - v1 [float]:                    maximal velocity in the biofilm [model parameter]
            - alpha [array]:                2D velocity map inside the biofilm  (alpha[b] = v0*(1-b) + v1*b)
            - gamma [float]:                penalisation coefficient [model parameter]
            - beta [float]:                    mean acceleration due to the gradient term [model parameter]
            - eps [float]:                    stochastic coefficient [model parameter]
            - swimmer_ID [list]:            swimmer_ID, for each swimmer
            - traj [dict]:                    summary of all swimmers information
            - swimmer_indexTime [list]:        time index list, for each swimmer
            - swimmer_time [list]:            real time list, for each swimmer
            - swimmer_totalTime [list]:        final time, for each swimmer
            - time_localIndex [list]:        local time index list, for each time
            - time_indexSwimmers [list]:    swimmer index list, for each time
            - time_totalSwimmers [list]:    total number of swimmers, for each time
            - swimmer_posX [list]:            X-positions list, for each swimmer
            - swimmer_posY [list]:            Y-positions list, for each swimmer
            - swimmer_Vx [list]:            X-velocity list, for each swimmer
            - swimmer_Vy [list]:            Y-velocity list, for each swimmer
            - swimmer_Ax [list]:            X-acceleration list, for each swimmer
            - swimmer_Ay  [list]:            Y-acceleration list, for each swimmer
            - swimmer_dist [list]:            total distance, for each swimmer
            - swimmer_dep [list]:            raw displacement, for each swimmer
            - swimmer_pers [list]:            persistence (distance / displacement), for each swimmer
            - swimmer_Vmean [list]:            mean velocity (magnitude), for each swimmer
            - swimmer_Vmax [list]:            maximum velocity (magnitude), for each swimmer
            - swimmer_Amean [list]:            mean acceleration (magnitude), for each swimmer
            - swimmer_Amax [list]:            maximum acceleration (magnitude), for each swimmer
            - swimmer_timeSwitchb [list]:    time to reach a 20% biofilm density change (respect to t=0), for each swimmer
            - swimmer_count [list]:            total number of swimmers
            - heatMap [array]:                2D heat map
            - spatialMap [array]:            2D trace map

            # global swimming descriptors
            - dist [float]:                    average distance
            - dep [float]:                    average displacement
            - pers [float]:                    average persistence
            - Vmean [float]:                average mean velocity
            - Vmax [float]:                    average maximum velocity
            - Amean [float]:                average mean acceleration
            - Amax [float]:                    average maximum acceleration
            - timeSwitchb [float]:            average time to reach a 20% biofilm density change
            - correlation_dotProd_Agradb_r [float]:     correlation coefficient (r) between A.gradb and gradb magnitude
            - norm_crossProd_Agradb_max [float]:        maximum of the cross-product norm between A and gradb
            - norm_crossProd_AV_max [float]:            maximum of the cross-product norm between A and V
            - correlation_Vb_r [float]:                    correlation coefficient (r) between V and b
            - normality_Ax [float]:                        normality test over the X-acceleration (minimum of shapiro and normaltest p-values: 0 = non normal)
            - normality_Ay [float]:                        normality test over the Y-acceleration (minimum of shapiro and normaltest p-values: 0 = non normal)
            - coveredArea [float]:            percentage of covered area over all swimmers

        methods:
            - init:                            definition of all class variables and initialisation of the parameters
            - initData_swimmers:            initialisation of swimming variables from a list of simulated swimmers
            - computeMapOccupancy :         Display the occupancy, i.e. the total surface covered by a bactery during its run 
            - filterSwimmersA:                filter the swimmers variables to remove the shortest trajectories
            - filterSwimmersTimeSteps:        filter the swimmers variables to remove all short trajectories
            - computeLocalDescriptors:        compute the local descriptors (for each swimmer)
            - computeGlobalDescriptors:        compute the global descriptors
            - GetIndex:                        return the index of a point in the underlying grid
            - isInList:                        check if the element is in the list
            - computeSpatialDescriptors:    compute the spatial descriptors
            - plotSwimmers:                    plot a matplotlib plot of the swimmers on the biofilm
    """


    def __init__(self, Tf, dt, dx, dy, m, n, length_x_inf, length_x_sup, length_y_inf, length_y_sup, b, gradb_x, gradb_y, v0, v1, gamma, beta, eps):
        """
        definition of all class variables and initialisation of the parameters
        Input:
             - Tf [int]:                        final time of all trajectories
            - dt [float]:                    time step
            - dx [float]:                    spatial step (x-direction)
            - dy [float]:                    spatial step (y-direction)
            - m [int]:                        x-index (number of pixels)
            - n [int]:                        y-index (number of pixels)
            - length_x_inf [float]:            x-origin
            - length_x_sup [float]:            x-boundary
            - length_y_inf [float]:            y-origin
            - length_y_sup [float]:            y-boundary
            - b [array]:                    biofilm density (image stack stored as [Ncycle x m x n])
            - gradb_x [array]:                gradient of the biofilm density (x-direction - [Ncycle x m x n])
            - gradb_y [array]:                gradient of the biofilm density (y-direction - [Ncycle x m x n])
            - v0 [float]:                    minimal velocity in the biofilm [model parameter]
            - v1 [float]:                    maximal velocity in the biofilm [model parameter]
             - gamma [float]:                penalisation coefficient [model parameter]
            - beta [float]:                    mean acceleration due to the gradient term [model parameter]
            - eps [float]:                    stochastic coefficient [model parameter]
        Output:
            void
        """

        #total number of swimmers
        self.swimmer_Nb = None
        self.dt = dt
        self.Tmax = Tf

        #map
        self.length_x_inf = length_x_inf
        self.length_x_sup = length_x_sup
        self.length_y_inf = length_y_inf
        self.length_y_sup = length_y_sup
        self.dx = dx
        self.dy = dy
        self.m = m
        self.n = n

        #biofilm
        self.b = b
        self.gradbX = gradb_x
        self.gradbY = gradb_y

        #model parameters
        self.v0 = v0
        self.v1 = v1
        self.alpha = np.zeros((self.m,self.n))
        self.gamma = gamma
        self.beta = beta
        self.eps = eps

        #particle ID list
        self.swimmer_ID = None

        #particle time
        self.traj = None
        self.swimmer_indexTime = None
        self.swimmer_time = None
        self.swimmer_totalTime = None

        self.time_localIndex = None
        self.time_indexSwimmers = None
        self.time_totalSwimmers = None
        #particle position
        self.swimmer_posX = None
        self.swimmer_posY = None
        #particle velocity
        self.swimmer_Vx = None
        self.swimmer_Vy = None
        #particle acceleration
        self.swimmer_Ax = None
        self.swimmer_Ay = None

        #local descriptors
        self.swimmer_dist = None
        self.swimmer_dep = None
        self.swimmer_pers = None
        self.swimmer_Vmean = None
        self.swimmer_Vmax = None
        self.swimmer_Amean = None
        self.swimmer_Amax = None
        #particle time to get over b_init (+/- 20%)
        self.swimmer_timeSwitchb = None
        self.swimmer_count = None

        #spatial descriptors
        self.heatMap = np.zeros((self.m,self.n))
        self.spatialMap = np.zeros((self.m,self.n))

        #global descriptors
        self.dist = None
        self.dep = None
        self.pers = None
        self.Vmean = None
        self.Vmax = None
        self.Amean = None
        self.Amax = None
        self.timeSwitchb = None

        self.correlation_dotProd_Agradb_r = None
        self.norm_crossProd_Agradb_max = None
        self.norm_crossProd_AV_max = None
        self.correlation_Vb_r = None
        self.normality_Ax = None
        self.normality_Ay = None
        self.coveredArea = None



    def initData_swimmers(self, swimmer_list):
        """
        initialisation of swimming variables from a list of simulated swimmers
        Calls:
            - Data.isInList
            - Data.GetIndex
        Input:
            - swimmer_list [list]:            list of swimmers
        Output:
            void
        """
        swimmer_ID_list = list()
        swimmer_time_list = list()
        swimmer_posX_list = list()
        swimmer_posY_list = list()
        swimmer_count_list = list()
        totalTime = list()
        for swimmer in swimmer_list:
            for j in np.unique(swimmer.trajectory.swimmer_ID):
                spatialMap = np.zeros((self.m,self.n))
                trajX = swimmer.trajectory.pos[swimmer.trajectory.swimmer_ID==j,0]
                trajY = swimmer.trajectory.pos[swimmer.trajectory.swimmer_ID==j,1]
                totalTime.append(trajX.shape[0])
                for k in range(trajX.shape[0]):
                   iX = trajX[k]
                   iY = trajY[k]
                   indexI, indexJ = self.GetIndex([iX,iY])
                   spatialMap[indexI,indexJ] = 1
                swimmer_ID_list.append(j)
                swimmer_count_list.append(np.sum(spatialMap))
                timeseries = swimmer.trajectory.time[swimmer.trajectory.swimmer_ID==j]
                swimmer_time_list.append(timeseries.reshape(timeseries.shape[0]))
                swimmer_posX_list.append(trajX)
                swimmer_posY_list.append(trajY)
        self.swimmer_ID = np.array(swimmer_ID_list)
        self.swimmer_Nb = len(swimmer_ID_list)
        self.swimmer_count = np.array(swimmer_count_list)
        self.swimmer_time = np.array(swimmer_time_list)
        self.swimmer_totalTime = np.array(totalTime,int)
        swimmer_Nt_list = [[round(t[i]/self.dt) for i in range(t.shape[0])] for t in swimmer_time_list]

        self.time_localIndex = np.zeros((self.Tmax,len(swimmer_Nt_list)),int)
        for indexs,s in enumerate(swimmer_Nt_list):
            for indext,t in enumerate(s):
                self.time_localIndex[t][indexs] = indext

        swimmer_Nt = swimmer_Nt_list
        #self.time_indexSwimmers = np.zeros((self.Tmax),int)
        alllist = list()
        self.time_totalSwimmers = np.zeros((self.Tmax),int)
        for t in range(self.Tmax):
                time_indexSwimmers = list()
                for indexS,s in enumerate(swimmer_Nt_list):
                    if self.isInList(t,s):
                        self.time_totalSwimmers[t] += 1
                        time_indexSwimmers.append(indexS)
                alllist.append(np.array(time_indexSwimmers))
        self.time_indexSwimmers = np.array(alllist)
        self.swimmer_indexTime = np.array(swimmer_Nt)
        self.swimmer_posX = np.array(swimmer_posX_list)
        self.swimmer_posY = np.array(swimmer_posY_list)
        swimmer_Vx_list = [np.array([(t[i+1]-t[i])/(self.swimmer_time[it][i+1]-self.swimmer_time[it][i]) for i in range(t.shape[0]-1)]) for it,t in enumerate(swimmer_posX_list)]
        self.swimmer_Vx = np.array(swimmer_Vx_list)
        swimmer_Vy_list = [np.array([(t[i+1]-t[i])/(self.swimmer_time[it][i+1]-self.swimmer_time[it][i]) for i in range(t.shape[0]-1)]) for it,t in enumerate(swimmer_posY_list)]
        self.swimmer_Vy = np.array(swimmer_Vy_list)
        swimmer_Ax_list = [np.array([(t[i+1]-t[i])/(self.swimmer_time[it][i+1]-self.swimmer_time[it][i]) for i in range(t.shape[0]-1)]) for it,t in enumerate(swimmer_Vx_list)]
        self.swimmer_Ax = np.array(swimmer_Ax_list)
        swimmer_Ay_list = [np.array([(t[i+1]-t[i])/(self.swimmer_time[it][i+1]-self.swimmer_time[it][i]) for i in range(t.shape[0]-1)]) for it,t in enumerate(swimmer_Vy_list)]
        self.swimmer_Ay = np.array(swimmer_Ay_list)



    def computeMapOccupancy(self,r=2,p=10,show=True):
        """
        Display the occupancy, i.e. the total surface covered by a bactery during its run
        Input:
            - r (default = 2):  cell radius
            - p (default = 10): nb of subsample of position in each trajectory piece.
        """
        import matplotlib.pyplot as plt
        if show:
            fig, ax = plt.subplots()
        self.spatialMap = np.zeros((self.swimmer_Nb))
        spatialMapAll = np.zeros((self.m,self.n))
        for j in range(self.swimmer_Nb):
            spatialMap = np.zeros((self.m,self.n))
            for k in range(len(self.swimmer_posX[j])-1):
                for t in range(p):
                    iX = (1-t*1.0/(p-1))*self.swimmer_posX[j][k] + t*1.0/(p-1)*self.swimmer_posX[j][k+1]
                    iY = (1-t*1.0/(p-1))*self.swimmer_posY[j][k] + t*1.0/(p-1)*self.swimmer_posY[j][k+1]
                    for l in range(2*r+1):
                        for ll in range(2*r+1):
                            indexI, indexJ = self.GetIndex([iX-(r-l)*self.dx, iY-(r-ll)*self.dx])
                            if (r==0):
                                spatialMap[indexI,indexJ] = 1
                                spatialMapAll[indexI,indexJ] = 1
                            elif ((-(r-l)*self.dx)**2 + (-(r-ll)*self.dx)**2 < r**2*self.dx**2):
                                spatialMap[indexI,indexJ] = 1
                                spatialMapAll[indexI,indexJ] = 1
            self.spatialMap[j] = spatialMap.sum()*self.dx**2
        if show:
            ax.imshow(np.flip(spatialMapAll,0), extent=(self.length_y_inf,self.length_y_sup,self.length_x_inf,self.length_x_sup), cmap='gray', interpolation='spline36')
            plt.show()


    def filterSwimmersA(self):
        """
        filter the swimmers variables to remove the shortest trajectories
        Calls:
            - Data.isInList
        Input:
            void
        Output:
            void
        """
        Ax = list()
        Ay = list()
        Vx = list()
        Vy = list()
        posX = list()
        posY = list()
        indexTime = list()
        time = list()
        count = list()
        ID = list()
        totalTime = list()
        for iA,Alist in enumerate(self.swimmer_Ax):
            if len(Alist)==0:
                self.swimmer_Nb -= 1
            else:
                Ax.append(self.swimmer_Ax[iA])
                Ay.append(self.swimmer_Ay[iA])
                Vx.append(self.swimmer_Vx[iA])
                Vy.append(self.swimmer_Vy[iA])
                posX.append(self.swimmer_posX[iA])
                posY.append(self.swimmer_posY[iA])
                indexTime.append(self.swimmer_indexTime[iA])
                time.append(self.swimmer_time[iA])
                count.append(self.swimmer_count[iA])
                ID.append(self.swimmer_ID[iA])
                totalTime.append(self.swimmer_totalTime[iA])
        self.swimmer_Vx = np.array(Vx)
        self.swimmer_Vy = np.array(Vy)
        self.swimmer_posX = np.array(posX)
        self.swimmer_posY = np.array(posY)
        self.swimmer_Ax = np.array(Ax)
        self.swimmer_Ay = np.array(Ay)
        self.swimmer_indexTime = np.array(indexTime)
        self.swimmer_time = np.array(time)
        self.swimmer_count = np.array(count)
        self.swimmer_ID = np.array(ID)
        self.swimmer_totalTime = np.array(totalTime)
        self.time_localIndex = np.zeros((885,len(self.swimmer_indexTime)),int)
        for indexs,s in enumerate(self.swimmer_indexTime):
            for indext,t in enumerate(s):
                self.time_localIndex[t][indexs] = indext
        alllist = list()
        self.time_totalSwimmers = np.zeros((self.Tmax),int)
        for t in range(self.Tmax):
                time_indexSwimmers = list()
                for indexS,s in enumerate(self.swimmer_indexTime):
                    if self.isInList(t,s):
                        self.time_totalSwimmers[t] += 1
                        time_indexSwimmers.append(indexS)
                alllist.append(np.array(time_indexSwimmers))
        self.time_indexSwimmers = np.array(alllist)



    def filterSwimmersTimeSteps(self):
        """
        filter the swimmers variables to remove all short trajectories
        Calls:
            - Data.isInList
        Input:
            void
        Output:
            void
        """
        Ax = list()
        Ay = list()
        Vx = list()
        Vy = list()
        posX = list()
        posY = list()
        indexTime = list()
        time = list()
        count = list()
        ID = list()
        totalTime = list()
        for iA,Alist in enumerate(self.swimmer_Ax):
            if self.swimmer_totalTime[iA]<8:
                self.swimmer_Nb -= 1
            else:
                Ax.append(self.swimmer_Ax[iA])
                Ay.append(self.swimmer_Ay[iA])
                Vx.append(self.swimmer_Vx[iA])
                Vy.append(self.swimmer_Vy[iA])
                posX.append(self.swimmer_posX[iA])
                posY.append(self.swimmer_posY[iA])
                indexTime.append(self.swimmer_indexTime[iA])
                time.append(self.swimmer_time[iA])
                count.append(self.swimmer_count[iA])
                ID.append(self.swimmer_ID[iA])
                totalTime.append(self.swimmer_totalTime[iA])
        self.swimmer_Vx = np.array(Vx)
        self.swimmer_Vy = np.array(Vy)
        self.swimmer_posX = np.array(posX)
        self.swimmer_posY = np.array(posY)
        self.swimmer_Ax = np.array(Ax)
        self.swimmer_Ay = np.array(Ay)
        self.swimmer_indexTime = np.array(indexTime)
        self.swimmer_time = np.array(time)
        self.swimmer_count = np.array(count)
        self.swimmer_ID = np.array(ID)
        self.swimmer_totalTime = np.array(totalTime)
        self.time_localIndex = np.zeros((885,len(self.swimmer_indexTime)),int)
        for indexs,s in enumerate(self.swimmer_indexTime):
            for indext,t in enumerate(s):
                self.time_localIndex[t][indexs] = indext
        alllist = list()
        self.time_totalSwimmers = np.zeros((self.Tmax),int)
        for t in range(self.Tmax):
                time_indexSwimmers = list()
                for indexS,s in enumerate(self.swimmer_indexTime):
                    if self.isInList(t,s):
                        self.time_totalSwimmers[t] += 1
                        time_indexSwimmers.append(indexS)
                alllist.append(np.array(time_indexSwimmers))
        self.time_indexSwimmers = np.array(alllist)





    def computeLocalDescriptors(self):
        """
        compute the local descriptors (for each swimmer)
        Input:
            void
        Output:
            void
        """
        #local descriptors
        self.swimmer_dist = np.array([np.sum(np.array([np.linalg.norm([self.swimmer_posX[s][t+1]-self.swimmer_posX[s][t],self.swimmer_posY[s][t+1]-self.swimmer_posY[s][t]]) for t in range(self.swimmer_posX[s].shape[0]-1) if self.swimmer_posX[s].shape[0]>1])) for s in range(self.swimmer_posX.shape[0])])

        self.swimmer_dep = np.array([np.linalg.norm([self.swimmer_posX[s][-1]-self.swimmer_posX[s][0],self.swimmer_posY[s][-1]-self.swimmer_posY[s][0]]) for s in range(self.swimmer_posX.shape[0])])

        self.swimmer_pers = np.array([self.swimmer_dist[s]/(self.swimmer_dep[s]+1e-12) for s in range(self.swimmer_dist.shape[0])])

        self.swimmer_Vmean = np.array([np.mean(np.array([np.linalg.norm([self.swimmer_Vx[s][i],self.swimmer_Vy[s][i]]) for i in range(self.swimmer_Vx[s].shape[0])])) for s in range(self.swimmer_Vx.shape[0]) if len([np.linalg.norm([self.swimmer_Vx[s][i],self.swimmer_Vy[s][i]]) for i in range(self.swimmer_Vx[s].shape[0])])>0])

        self.swimmer_Vmax = np.array([np.max(np.array([np.linalg.norm([self.swimmer_Vx[s][i],self.swimmer_Vy[s][i]]) for i in range(self.swimmer_Vx[s].shape[0])])) for s in range(self.swimmer_Vx.shape[0]) if len([np.linalg.norm([self.swimmer_Vx[s][i],self.swimmer_Vy[s][i]]) for i in range(self.swimmer_Vx[s].shape[0])])>0])

        self.swimmer_Amean = np.array([np.mean(np.array([np.linalg.norm([self.swimmer_Ax[s][i],self.swimmer_Ay[s][i]]) for i in range(self.swimmer_Ax[s].shape[0])])) for s in range(self.swimmer_Ax.shape[0]) if len([np.linalg.norm([self.swimmer_Ax[s][i],self.swimmer_Ay[s][i]]) for i in range(self.swimmer_Ax[s].shape[0])])>0])

        self.swimmer_Amax = np.array([np.max(np.array([np.linalg.norm([self.swimmer_Ax[s][i],self.swimmer_Ay[s][i]]) for i in range(self.swimmer_Ax[s].shape[0])])) for s in range(self.swimmer_Ax.shape[0]) if len([np.linalg.norm([self.swimmer_Ax[s][i],self.swimmer_Ay[s][i]]) for i in range(self.swimmer_Ax[s].shape[0])])>0])


    def computeGlobalDescriptors(self):
        """
        compute the global descriptors
        Input:
            void
        Output:
            void
        """
        #global descriptors
        self.dist = np.mean(self.swimmer_dist)
        self.dep = np.mean(self.swimmer_dep)
        self.pers = np.mean(self.swimmer_pers)
        self.Vmean = np.mean(self.swimmer_Vmean)
        self.Vmax = np.max(self.swimmer_Vmax)
        self.Amean = np.mean(self.swimmer_Amean)
        self.Amax = np.max(self.swimmer_Amax)
        self.coveredArea = np.mean(self.swimmer_count)/(float(self.m)*float(self.n))*100



    def GetIndex(self,pts):
        """
        return the index of a point in the underlying grid
        Input:
            - pts [array]:                point coordinates
        Output:
            - index array of the point
        """
        i = min(max(0,int((pts[0]-self.length_x_inf-self.dx/2)/self.dx)),self.m-1)
        j = min(max(0,int((pts[1]-self.length_y_inf-self.dy/2)/self.dy)),self.n-1)
        return [i,j]



    def isInList(self,i,L):
        """
        check if the element is in the list
        Input:
            - i [template]:                element
            - L [list]:                    list
        Output:
            - boolean        
        """
        boolean = 0
        for j in L:
            if j==i:
                boolean = 1
        return boolean



    def computeSpatialDescriptors(self):
        """
        compute the spatial descriptors
        Calls:
            - Data.GetIndex
        Input:
            void
        Output:
            void        
        """
        self.heatMap = np.zeros((self.m,self.n))
        self.spatialMap = np.zeros((self.m,self.n))
        self.swimmer_timeSwitchb = list()
        #spatial descriptors
        for s in range(self.swimmer_posX.shape[0]):
            swimmer_timeSwitchb = 0.0
            boolSwitchb = 0
            indexI0, indexJ0 = self.GetIndex([self.swimmer_posX[s][0],self.swimmer_posY[s][0]])
            b0 = self.b[indexI0,indexJ0]
            for i in range(self.swimmer_posX[s].shape[0]):
                iX = self.swimmer_posX[s][i]
                iY = self.swimmer_posY[s][i]
                indexI, indexJ = self.GetIndex([iX,iY])
                self.heatMap[indexI,indexJ] += 1
                self.spatialMap[indexI,indexJ] = 1
                b = self.b[indexI,indexJ]
                if (np.abs(b-b0)<0.2*np.abs(b0))and(i>0)and(boolSwitchb==0):
                    swimmer_timeSwitchb += self.swimmer_time[s][i]-self.swimmer_time[s][i-1]
                elif (i>0):
                    boolSwitchb = 1
            self.swimmer_timeSwitchb.append(swimmer_timeSwitchb)
        self.heatMap[0,:] = 0
        self.heatMap[-1,:] = 0
        self.heatMap[:,0] = 0
        self.heatMap[:,-1] = 0

        self.swimmer_timeSwitchb = np.array(self.swimmer_timeSwitchb)
        self.timeSwitchb = np.mean(self.swimmer_timeSwitchb)



    def plotSwimmers(self):
        """
        plot a matplotlib plot of the swimmers on the biofilm
        Input:
            void
        Output:
            void
        """
        import matplotlib.pyplot as plt
        Xref = 1.0 #0.45
        fig, ax = plt.subplots()
        ax.imshow(np.flip(self.b,0), extent=(self.length_y_inf,self.length_y_sup,self.length_x_inf,self.length_x_sup), cmap='gray', interpolation='spline36')

        Nswimmers = self.swimmer_Nb
        for Id,IDval in enumerate(self.swimmer_ID):
            ax.plot(self.swimmer_posY[Id]*Xref,self.swimmer_posX[Id]*Xref,'-',linewidth=0.6,     c=plt.cm.jet(int(255*Id/Nswimmers)),marker='o',markersize=1, 
markevery=1000000000,markerfacecolor=plt.cm.jet(int(255*Id/Nswimmers)),alpha=1)

        ax.set_ylim(self.length_x_inf, self.length_x_sup)
        ax.set_xlim(self.length_y_inf, self.length_y_sup)
        ax.set(xticks=[],yticks=[])
        plt.show()



def plotSwimmers(data_batch_list, ax, species, dict_color={'Pumilus':'Blues','Sphaericus':'Oranges','Cereus':'Greens'}, Title=True):
    """
    Plot swimmers trajectories colored by species
    
    input: 
        - data_batch_list : a list of data (the different batches)
        - ax : a matplotlib axis to plot in
        - species : the name of the species
        - dict_color : a dictionnary of matplotlib colormap names.
        - Title (bool) : tell if plot title is displayed
    """
    import matplotlib.pyplot as plt
    cm = {k:plt.cm.__dict__[k_i] for k,k_i in dict_color.items()}

    Nswimmers = np.sum(np.array([d.swimmer_Nb for d in data_batch_list]))
    curr_ID=0
    
    for d in data_batch_list:        
        for Id,IDval in enumerate(d.swimmer_ID):
        #128+int(128*(curr_ID)/Nswimmers
            ax.plot(d.swimmer_posY[Id] -d.length_y_inf ,d.swimmer_posX[Id] -d.length_x_inf, '-' ,linewidth=1., c=cm[species](128),marker='o',markersize=1, 
    markevery=1000000000,markerfacecolor=cm[species](128),alpha=1)
            curr_ID+=1
    print('number of trajectory for species',species,curr_ID,Nswimmers)
    if Title:
        ax.set_title(r'$B.'+species.lower()+'$')
    ax.set_ylim(0, data_batch_list[0].m*data_batch_list[0].dx)
    ax.set_xlim(0, data_batch_list[0].n*data_batch_list[0].dy)
    ax.set(xticks=[],yticks=[])
    ax.set_aspect('equal')


def plotRunTumble(data_batch_list, axs, species, Vmean, ratio=3, quantiles=[5,50,95], dict_color={'Pumilus':'Blues','Sphaericus':'Oranges','Cereus':'Greens'}, Title=True, distance_threshold=0.3, YLABEL=True):
    """
    Plot run and tumble for each species, colored by species
    
    input: 
        - data_batch_list : a list of data (the different batches)
        - ax : a 2x2 array of matplotlib axis to plot in
        - species : the name of the species
        - Vmean : normalization value
        - dict_color : a dictionnary of matplotlib colormap names.
        - Title (bool) : tell if plot title is displayed
    """
    import matplotlib.pyplot as plt
    from post_process2D import ComputePercentile1D
    import pandas as pd

    ccol = {s:'C'+str(i_s) for i_s, s in enumerate(dict_color.keys())}

    cm = {k:plt.cm.__dict__[k_i] for k,k_i in dict_color.items()}

    Theta_all=np.array([])
    Curr_V_norm_mean_all=np.array([])
    for d in data_batch_list:
        for Id,IDval in enumerate(d.swimmer_ID):          
            Curr_V = np.array([d.swimmer_Vx[Id],d.swimmer_Vy[Id]])
            
            Curr_V_norm = np.linalg.norm(Curr_V,axis=0) 
            #filtering out small velocities
            #minimal microscope precision is 0.1. We filter out speeds inferior to norm((0.1/dt,0.1/dt))
            dV = np.linalg.norm(np.array([distance_threshold/d.dt,distance_threshold/d.dt]))
            Curr_V = Curr_V[:,Curr_V_norm>dV]            
            Curr_V_norm = Curr_V_norm[Curr_V_norm>dV]
            Curr_V_norm_mean = ((Curr_V_norm[1:]+Curr_V_norm[:-1])/2)/Vmean
#            Curr_V_norm_mean = Curr_V_norm[1:]/Vmean
            Theta = np.rad2deg(np.arccos(np.clip(np.diag(np.dot(Curr_V[:,:-1].T,Curr_V[:,1:]))/(np.linalg.norm(Curr_V[:,:-1],axis=0)*np.linalg.norm(Curr_V[:,1:],axis=0)),-1,1)))


            Curr_V_norm_mean_all = np.hstack([Curr_V_norm_mean_all,Curr_V_norm_mean])
            Theta_all = np.hstack([Theta_all,Theta])
            
    axs[0][1].scatter(Curr_V_norm_mean_all ,Theta_all, c=[cm[species](128) for l in Curr_V_norm_mean_all],marker='o',s=4, alpha=0.5,linewidths=0.3)
    Stats={"Theta":{},'V':{}}
    Percentile_out=['percentile_val_sp', 'mean_sp', 'kde_sp', 'max_val_dist','x0']
    Stats["Theta"] ={p:r for p,r in zip(Percentile_out,ComputePercentile1D(pd.DataFrame({"value":Theta_all,"Species":species})))}
    Stats["V"] ={p:r for p,r in zip(Percentile_out,ComputePercentile1D(pd.DataFrame({'value':Curr_V_norm_mean_all,'Species':species})))}


    if Title:
        axs[0][1].set_title(r'$B.'+species.lower()+'$')
    axs[0][1].set_ylim(0, 180)
    axs[0][1].set_xlim(dV/Vmean, ratio)#Stats['V']['percentile_val_sp'][species][95])
#    axs[0][1].set_xlim(0.9*Stats['V']['percentile_val_sp'][species][5],1.1*Stats['V']['percentile_val_sp'][species][95])#Stats['V']['percentile_val_sp'][species][95])    
    axs[0][1].set(xticks=[],yticks=[])
    y = np.linspace(0,180,100)
    axs[0][0].fill_betweenx(y,0,Stats['Theta']['kde_sp'][species](y),color=ccol[species],            
                                alpha=0.4, lw=0.5)
    axs[0][0].plot(Stats['Theta']['kde_sp'][species](y),y,color=ccol[species],            
                                 lw=1)
    maxx=Stats['Theta']['kde_sp'][species](y).max()
    for q in quantiles:
        axs[0][0].plot([0,maxx],[Stats['Theta']['percentile_val_sp'][species][q] for i in range(2)],
                        linewidth=1,zorder=2,color=ccol[species])
    axs[0][0].plot([0,maxx],[Stats['Theta']['mean_sp'][species] for i in range(2)],
                        linewidth=1,zorder=2,color=ccol[species],ls='--')
    axs[0][0].invert_xaxis()
    axs[0][0].set(xticks=[],yticks=[0,90,180])
    axs[0][0].set_ylim(0, 180)
    if YLABEL:        
        axs[0][0].set_ylabel(r'$\theta^s_i(t)$')
    x = np.linspace(dV/Vmean,ratio,100)                                
    axs[1][1].fill_between(x,0,Stats['V']['kde_sp'][species](x),color=ccol[species],            
                                alpha=0.4, lw=0.5)
    axs[1][1].plot(x,Stats['V']['kde_sp'][species](x),color=ccol[species],            
                                lw=1)                                
    axs[1][1].invert_yaxis()
    axs[1][1].set_xlim(dV/Vmean, ratio)# Stats['V']['percentile_val_sp'][species][95])
#    axs[1][1].set_xlim(0.9*Stats['V']['percentile_val_sp'][species][5],1.1*Stats['V']['percentile_val_sp'][species][95])#Stats['V']['percentile_val_sp'][species][95])     
    axs[1][1].set(yticks=[])        
    axs[1][1].set_xlabel(r'$\bar{V}^s_i(t)$')
    maxy=Stats['V']['kde_sp'][species](x).max()
    for q in quantiles:
        axs[1][1].plot([Stats['V']['percentile_val_sp'][species][q] for i in range(2)],[0,maxy],
                        linewidth=1,zorder=2,color=ccol[species])
    axs[1][1].plot([Stats['V']['mean_sp'][species] for i in range(2)],[0,maxy],
                    linewidth=1,zorder=2,color=ccol[species],ls='--')        
    axs[1][0].axis('off')




