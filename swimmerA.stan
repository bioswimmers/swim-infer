/****************************************************************************/
/*	STAN MODEL: swimmerAbis 																										*/
/*	regression model on the acceleration, to determine the model parameters */
/*	(no storage of the full matrices)					*/
/*	(CSR-like data structure switched)					*/
/*	inference data:	accelerations at all time steps, for each swimmer  			*/
/*	other data:	positions and velocity at all time steps, for each swimmer		*/
/*	data structure:	- CSR-like structures																		*/
/*								used to avoid matrix structures,													*/
/*								similarly to the CSR format																*/



	/**************************************************************************/
	/*	FUNCTIONS block																												*/
	/*	Contents:																															*/
	/*			- Norm:			compute the Euclidian norm from two reals							*/
	/*			- real2int:		convert a real to an integer (floor function)				*/
	/*			- GetIndex:	get index in biofilm matrix of swimmer position 		*/
	/**************************************************************************/
	functions {

		/************************************************************************/
		/*	Norm: 							 																								*/
		/*			compute the Euclidian norm from two reals													*/
		/*	Input:																															*/
		/*			- x [real]:							abscissa 													*/
		/*			- y [real]:							ordinate 													*/
		/*	Output:																															*/
		/*			- Euclidian norm [real]												*/
		real Norm(real x, real y) {						
			return sqrt(square(x) + square(y));
		}

		/************************************************************************/
		/*	real2int: 					 																								*/
		/*			convert a real to an integer (floor function)										*/
		/*	Input:																															*/
		/*			- x [real]:							real to be converted										*/
		/*	Output:																															*/
		/*			- ix [int]:							floor value															*/
		int real2int(real x) { 
		  // floor function converted to int (no Stan function provided)
			int ix = 0;
			int res = 0;
			int boolean = 0;
			while (boolean==0) {
				if (x<ix+1) {
					res = ix;
					boolean = 1;
				}
				else {
					ix = ix+1;
				}
			}
			return ix;
		}

		/************************************************************************/
		/*	GetIndex: 					 																								*/
		/*			get index (i,j) in biofilm matrix of swimmer position (x,y)			*/
		/*	Calls:																															*/
		/*			- real2int																											*/
		/*	Input:																															*/
		/*			- x [real]:						abscissa 															*/
		/*			- y [real]:						ordinate 															*/
		/*			- x_inf [real]:					x-origin																*/
		/*			- y_inf [real]:					y-origin																*/
		/*			- dx [real]:						spatial step (x-direction)							*/
		/*			- dy [real]:						spatial step (y-direction)							*/
		/*			- M [int]:							x-index (number of pixels)							*/
		/*			- N [int]:							y-index (number of pixels)							*/
		/*	Output:																															*/
		/*			- index [vector]:	index vector 														*/
		/*															(concatenated structure)								*/
		vector GetIndex(real x, real y, real x_inf, real y_inf, real dx, real dy, int M, int N) {
			vector[2] index;
			vector[2] tmpMax;
			vector[2] tmpMin;
			tmpMax[1] = 0.0;
			tmpMax[2] = floor((x-x_inf-dx/2)/dx);
			tmpMin[1] = max(tmpMax);
			tmpMin[2] = (M-1)*1.0; // conversion to real and maximal index
			index[1] = real2int(min(tmpMin)+1);			// i = min(N-1,max(0,(x-x_inf-dx/2)/dx))
			tmpMax[2] = floor((y-y_inf-dy/2)/dy);
			tmpMin[1] = max(tmpMax);
			tmpMin[2] = (N-1)*1.0;
			index[2] = real2int(min(tmpMin)+1);			// j= min(M-1,max(0,(y-y_inf-dy/2)/dy))
			return index;
		}
	}
	
	



	/**************************************************************************/
	/*	DATA block																														*/
	/*	Contents:																															*/
	/*		- zNb [int]:				number of swimmer positions to be inferred			*/
	/*														[<= Ns x TT]																*/
	/*		- NSwimObs [int]:		size of the position matrix 										*/
	/*												(numberOfSwimmers x numberOfTimeSteps) [TT*Ns]	*/
	/*		- TT [int]:         number of time steps [Ncycles+1]								*/
	/*														time observations of the biofilm						*/
	/*		- Ns [int]:         total number of swimmers												*/
	/*		- M [int]:          x-index (number of pixels)											*/
	/*		- N [int]:          y-index (number of pixels)											*/
	/*		- x_inf [real]:    	x-origin																				*/
	/*		- y_inf [real]:    	y-origin																				*/
	/*		- dx [real]:        spatial step (x-direction)											*/
	/*		- dy [real]:				spatial step (y-direction)											*/
	/*		- dt [real]:        time step																				*/
	/*		- yVx [vector]:      velocity of all swimmers, at each time step (x-direction)			*/
	/*												(CSR-like vector)																*/
	/*		- yVy [vector]:      velocity of all swimmers, at each time step (y-direction)			*/
	/*												(CSR-like vector)																*/
	/*		- yB [vector]:      biofilm density of all swimmers, at each time step			*/
	/*												(CSR-like vector)																*/
	/*		- yGradBx [vector]:      gradient of the biofilm density of all swimmers, at each time step (x-direction)			*/
	/*												(CSR-like vector)																*/
	/*		- yGradBy [vector]:      gradient of the biofilm density of all swimmers, at each time step (y-direction)			*/
	/*												(CSR-like vector)																*/
	/*		- yAx [vector]:			accelerations of all swimmers, at each time step (x-direction) */
	/*												(CSR-like vector)																*/
	/*		- yAy [vector]:			accelerations of all swimmers, at each time step (y-direction) */
	/*												(CSR-like vector)																*/
	/*		- tt_swimmer [vector]:		global time indices from local time index, for for each swimmer																							*/
	/*												(CSR-like vector)																*/
	/*		- swimmer_indexA [vector]:	data structure which contains the index 	*/
	/*													of the first information in swimmers accelerations*/
	/*													for a given swimmer													*/
	/*												(CSR-like vector)																*/
	/*		- swimmer_indexV [vector]:	data structure which contains the index 	*/
	/*													of the first information in swimmers velocities*/
	/*													for a given swimmer													*/
	/*												(CSR-like vector)																*/
	/*		- swimmer_index [vector]:	data structure which contains the index 	*/
	/*													of the first information in swimmers positions*/
	/*													for a given swimmer													*/
	/*												(CSR-like vector)																*/
	/**************************************************************************/
	data {
	   real Xref;
	   real Vref;
	   real Aref;
	   int zNb;
	   int<lower=0> TT;                      // Number of time observation of the biofilm
	   int<lower=0> Ns;                      // Number of swimmers
	   int<lower=0> NSwimObs;                // Number of swimmer observations
	   int<lower=0> M;                       // Number of voxels in the x direction of the biofilm image
	   int<lower=0> N;                       // Number of voxels in the y direction of the biofilm image
	   real x_inf;                           // x origin of the biofilm image
	   real y_inf;                           // y origin of the biofilm image
	   real<lower=0> dx;                     // Space step in the x direction of the biofilm image
	   real<lower=0> dy;                     // Space step in the y direction of the biofilm image
	   real<lower=0> dt;                     // Time step in the swimmer trajectories
	   vector[zNb] yB; // Swimmer position (x coordinate): 
	   vector[zNb] yGradBx; // Swimmer position (x coordinate): 
	   vector[zNb] yGradBy; // Swimmer position (x coordinate): 
	   vector[zNb-2*Ns] yAx;              // swimmer acceleration
	   vector[zNb-2*Ns] yAy;              // swimmer acceleration
	   vector[zNb-Ns] yVx;              // swimmer velocity
	   vector[zNb-Ns] yVy;              // swimmer velocity
	   vector[zNb] tt_swimmer;      // index in the time vector of the corresponding position
	   vector[Ns+1] swimmer_index; // index in the position and tt_swimer positions of the first information of a given swimmer (cf CSR matrix)
	   vector[Ns+1] swimmer_indexV; // index in the velocity and tt_swimer positions of the first information of a given swimmer (cf CSR matrix)
	   vector[Ns+1] swimmer_indexA; // index in the acceleration and tt_swimer positions of the first information of a given swimmer (cf CSR matrix)
   }




	/**************************************************************************/
	/*	PARAMETERS block																											*/
	/*	Contents:																															*/
	/*		- v0 [real]:				minimal velocity in the biofilm									*/
	/*												(nominal speed)																	*/
	/*		- v1 [real]:				maximal velocity in the biofilm									*/
	/*												(nominal speed)																	*/
	/*		- gamma [ream]:			penalisation coefficient												*/
	/*												(friction term)																	*/
	/*		- beta [real]:			mean acceleration due to the gradient term			*/
	/*												(intensity of gradient detection)								*/
	/*		- sigma [vector]:		standard deviation of the global noise					*/
	/*												(per individual)																*/
	/**************************************************************************/
	parameters {
		real<lower=0> sigma0;            // standard deviation of the observation noise
		real<lower=0> v0;             // nominal speed in biofilm
		real<lower=0> v1;             // nominal speed in the extracellular matrix
		real<lower=0> gamma; // relaxation time of the friction term
		real beta;           // intensity of gradient detection
	}


	/**************************************************************************/
	/*	MODEL block																														*/
	/*	main steps:																														*/
	/*  ----Useful variables----																							*/
	/*	-----Initialisation-----																							*/
	/*	--Priors distributions--																							*/
	/*	-------Data model-------																							*/
	/*	--------Time loop-------																							*/
	/*	----Hidden true state---																							*/
	/*	-----Log-likelihood-----																							*/
	/**************************************************************************/
	model {
			 /*********************************************************************/
			 /*	Useful variables 					 																				*/
		   real myzero;
		   int it0Y;
		   int it0V;
		   int it0;
		   int it0p1;
		   real V;
		   real G;
		   real gbx;
		   real gby;
		   //vector[zNb-2*Ns] Ax;              // swimmer acceleration
		   //vector[zNb-2*Ns] Ay;              // swimmer acceleration

			 /*********************************************************************/
			 /*	Initialisation 						 																				*/
		   myzero = pow(10,-12);

			 /*********************************************************************/
			 /*	Priors distributions 						 																	*/
		   //sigma ~ normal(0,1);
		   sigma0 ~ normal(0,1);
		   gamma ~ normal(0,1);
		   v0 ~ normal(0,1);
		   v1 ~ normal(0,1);
		   beta ~ normal(0,1); //uniform(0,10); //normal(0,1);

			 /*********************************************************************/
			 /*	Data model											 																	*/
		   for (s in 1:Ns){
		   	   it0Y = real2int(swimmer_index[s]);
		   	   it0V = real2int(swimmer_indexV[s]);
		   	   it0 = real2int(swimmer_indexA[s]);
			   it0p1 = real2int(swimmer_indexA[s+1]);


			 /*********************************************************************/
			 /*	Time loop												 																	*/
			   for (t in it0:it0p1-1){
				V = Norm(yVx[it0V+t-it0],yVy[it0V+t-it0]);
				gbx = yGradBx[it0Y+t-it0+1];
				gby = yGradBy[it0Y+t-it0+1];
				G = Norm(gbx,gby);

			 /*********************************************************************/
			 /*	Log-likelihood									 																	*/
				yAx[t] ~ normal((gamma*(v0 + yB[it0Y+t-it0+1]*(v1-v0) - V)*yVx[it0V+t-it0]/(V+myzero) + beta*gbx/(G+myzero)) , sigma0);
				yAy[t] ~ normal((gamma*(v0 + yB[it0Y+t-it0+1]*(v1-v0) - V)*yVy[it0V+t-it0]/(V+myzero) + beta*gby/(G+myzero)) , sigma0);
                           }
		   }

	   }


/****************************************************************************/
/****************************************************************************/
