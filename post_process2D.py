#!/usr/bin/python
"""
This file contains different post-processing routines.
It mainly reads or writes image file (or an hdf5 version of a grayscale conversion of an image)
of the host biofilm.

Contents:
    - DataProcessing:            post-process the swimming data either from simulations or experiments
    - Data class:                compute the swimming data outputs including data descriptors from the whole set of swimmer trajectories
                                (see the complete descripton below, ~line 540)
"""

## Load library
import numpy as np
import h5py
from scipy import stats
from DataStructure import Data
import pandas as pd
import matplotlib.pyplot as plt




def DataProcessing(swimmer_list_loc):
    """
    post-process the swimming data either from simulations or experiments
    Calls:
        - Data
        - Data.initData_swimmers
        - Data.filterSwimmersA
        - Data.filterSwimmersTimeSteps
        - Data.computeLocalDescriptors
        - Data.computeSpatialDescriptors
        - Data.computeGlobalDescriptors
        - Data.plotSwimmers
    Input:
        - swimmer_list_loc [list]:        list of swimmers
    Output:
        - output [Data class]:            swimming data computed from simulated swimmers
    """
    output = Data(swimmer_list_loc[0].trajectory.time.shape[0], swimmer_list_loc[0].trajectory.dt, swimmer_list_loc[0].trajectory.dx, swimmer_list_loc[0].trajectory.dy, swimmer_list_loc[0].trajectory.m, swimmer_list_loc[0].trajectory.n, swimmer_list_loc[0].trajectory.length_x_inf, swimmer_list_loc[0].trajectory.length_x_sup, swimmer_list_loc[0].trajectory.length_y_inf, swimmer_list_loc[0].trajectory.length_y_sup,  swimmer_list_loc[0].trajectory.b, swimmer_list_loc[0].trajectory.gradb_x, swimmer_list_loc[0].trajectory.gradb_y, swimmer_list_loc[0].trajectory.v0, swimmer_list_loc[0].trajectory.v1, swimmer_list_loc[0].trajectory.gamma, swimmer_list_loc[0].trajectory.beta, swimmer_list_loc[0].trajectory.eps)
    output.initData_swimmers(swimmer_list_loc)
    output.filterSwimmersA()
    output.filterSwimmersTimeSteps()
    output.computeLocalDescriptors()
    output.computeSpatialDescriptors()
    output.computeGlobalDescriptors()
    output.plotSwimmers()
    return output



def generate_heatmap_data(kde, gx0, gy0, scale,mesh_size=127):
    """
    return the value of the function kde in a ternary plot
    
    Input:
        - kde : a function
        - gx0 : a mesh grid (x value)
        - gy0 : a mesh grid (y value)
        - scale : scale of the ternary plot
        - mesh_size : size of the mesh grid 
    Output:
        - d : a dictionnary mapping a ternary plot point to its value (kde).   
    """
    from ternary.helpers import simplex_iterator
    import math
    d = dict()
    for (i, j, k) in simplex_iterator(scale):
        x = 0.5*(2*i/scale + j/scale)
        y = 0.5*np.sqrt(3) * j/scale
        d[(i, j, k)] = kde([gx0[int(x*mesh_size),int(y*mesh_size)], gy0[int(x*mesh_size), int(y*mesh_size)]])
    return d



def ReformateDataForVizualisation(Param, dict_data, Aref, 
                                    Vref, Xref, Dref, Depref, 
                                    species_name='Ground truth',control=False):
    """
    Put the data in needed format for visualisation
    
    Input:
        - Param [dictionnary]:                          Simulation parameter dictionnary    
        - dict_data [dictionnary]:                      must contains the following fields
            mandatory fields (for swimmer in biofilm and without biofilm control)
            - YVxa [vect]:                                   velocity (x-direction)
            - YVya [vect]:                                   velocity (y-direction)
            - YAx [vect]:                                   acceleration (x-direction)
            - YAy [vect]:                                   acceleration (y-direction)
            - data [data array]:                            whole data set
            -Nta [int]:                                          data length
            accessory fields (for swimmer in biofilm only)        
            - YBa [vect]:                                    biofilm density
            - YgradBxa [vect]:                               gradient of the biofilm density (x-direction)
            - YgradBya [vect]:                               gradient of the biofilm density (y-direction)    
        - Aref [float]:                                 reference value for acceleration
        - Vref [float]:                                 reference value for velocity
        - Xref [float]:                                 reference value for position
        - Dref [float]:                                 reference value for distance
        - Depref [float]:                                 reference value for displacement        
        - species_name [str]:                           species name
        - control [bool] :                              True : control experiment (without biofilm), False : normal experiment (in biofilm).
        
    Output:
        - data_visu [dict]                              dict of data
    """
    if 'Nta' in dict_data.keys():
        Nta = dict_data['Nta']
    else:
        Nta = None
    if control == False:
        dB = {
        'b':dict_data['YBa'], 'gradBx':dict_data['YgradBxa'], 'gradBy':dict_data['YgradBya'],
        'gradB':np.sqrt(dict_data['YgradBxa']**2+dict_data['YgradBya']**2),
        'Species':[species_name for i in range(Nta)]}
        dfB = pd.DataFrame(data=dB)
        dfB = pd.melt(dfB, dfB.columns[-1], dfB.columns[:-1])

    dAx = {'Ax':dict_data['YAx']/Aref,'Species':[species_name for i in range(Nta)]}
    dfAx = pd.DataFrame(data=dAx)
    dfAx = pd.melt(dfAx, dfAx.columns[-1], dfAx.columns[:-1])

    dAy = {'Ay':dict_data['YAy']/Aref,'Species':[species_name for i in range(Nta)]}
    dfAy = pd.DataFrame(data=dAy)
    dfAy = pd.melt(dfAy, dfAy.columns[-1], dfAy.columns[:-1])

    dA = {'A':np.sqrt(dict_data['YAx']**2+dict_data['YAy']**2)/Aref,'Species':[species_name for i in range(Nta)]}
    dfA = pd.DataFrame(data=dA)
    dfA = pd.melt(dfA, dfA.columns[-1], dfA.columns[:-1])

    dAs = {
    'A':dict_data['data'].swimmer_Amean/Aref,
    'Species':[species_name for i in range(dict_data['data'].swimmer_Nb)]
    }
    dfAs = pd.DataFrame(data=dAs)
    dfAs = pd.melt(dfAs, dfAs.columns[-1], dfAs.columns[:-1])


    dVa = {
    'V':np.sqrt(dict_data['YVxa']**2+dict_data['YVya']**2)/Vref,
    'Species':[species_name for i in range(Nta)]
    }
    dfVa = pd.DataFrame(data=dVa)
    dfVa = pd.melt(dfVa, dfVa.columns[-1], dfVa.columns[:-1])



    dVs = {
        'V':dict_data['data'].swimmer_Vmean/Vref,
        'Species':[species_name for i in range(dict_data['data'].swimmer_Nb)]
        }
    dfVs = pd.DataFrame(data=dVs)
    dfVs = pd.melt(dfVs, dfVs.columns[-1], dfVs.columns[:-1])

    d = {
    'Dist':dict_data['data'].swimmer_dist/Dref,
    'Disp':dict_data['data'].swimmer_dep/Depref,
    'Area':dict_data['data'].spatialMap/(Dref**2),
    'Duration':dict_data['data'].swimmer_totalTime,
    'Species':[species_name for i in range(dict_data['data'].swimmer_Nb)]
    }
    dfDist = pd.DataFrame(data=d)
    dfDist = pd.melt(dfDist, dfDist.columns[-1], dfDist.columns[:-1])

    if control == False:    
        data_visu = {
                    r'$||A||$':dfAs, r'$||V||$':dfVs, 'dist':dfDist[dfDist.variable=="Dist"], 'disp':dfDist[dfDist.variable=="Disp"], 
                    "b":dfB[dfB.variable=="b"], "gradBx":dfB[dfB.variable=="gradBx"], "gradBy":dfB[dfB.variable=="gradBy"], 
                    "Ax":dfAx,"Ay":dfAy,r'$||\nabla b||$':dfB[dfB.variable=="gradB"], 'A': dfA, 'V': dfVa,
                    'Area': dfDist[dfDist.variable=="Area"],'Duration':dfDist[dfDist.variable=="Duration"]
                    }
    else:
        data_visu = {
                    r'$||A||$':dfAs, r'$||V||$':dfVs, 'dist':dfDist[dfDist.variable=="Dist"], 
                    'disp':dfDist[dfDist.variable=="Disp"],
                    "Ax":dfAx, "Ay":dfAy, 'A': dfA, 'V': dfVa,
                    'Area': dfDist[dfDist.variable=="Area"],'Duration':dfDist[dfDist.variable=="Duration"]
                    }    
    return data_visu
 

 

def ComputePercentile1D(x,percentiles=[1,5,25,50,75,95,99]):
    """
    Compute percentile, kde and max for a serie of n_species 1D dataset
    
    Input:
    - x [pandas df]                     1D dataset
    - percentiles [list]                Percentiles to be displayed
    
    Output:
    - percentile_val_sp [dictionnary]      Dictionnary of percentile values
    - kde_sp [dictionnary]                 Dictionnary of kdes
    - mean_sp [dictionnary]                Dictionnary of mean value
    - max_val_dist [float]                 KDE maximal value among species
    - x0 [array]                           sampling between min and max values           
    """
    x0 = np.linspace(np.min(x["value"]),np.max(x["value"]),500)
    max_val_dist = 0.0
    percentile_val_sp={}
    kde_sp={}
    mean_sp = {}
    number = {}
    for ie,hue in enumerate(x["Species"].unique()):
        d = x[x["Species"] == hue]
        number[hue]=d.shape[0]
        percentile_val_sp[hue] = {p:np.percentile(d["value"],p) for p in percentiles}
        mean_sp[hue] = np.mean(d["value"])
        kde_sp[hue] = stats.gaussian_kde(d["value"])
        max_val_dist=np.max([max_val_dist,np.max(kde_sp[hue](x0))])
    return percentile_val_sp, mean_sp, kde_sp, max_val_dist,x0, number


def ComputePercentile2D(x,y,percentiles=[1,5,25,50,75,95,99]):
    """
    Compute percentile, kde and max for a serie of n_species 2D dataset
    
    Input:
    - x [pandas df]                     first 1D dataset
    - y [pandas df]                     second 2D dataset
    - percentiles [list]                Percentiles to be displayed
    
    Output:
    - percentile_val_sp [dictionnary]      Dictionnary of percentile values
    - kde_sp [dictionnary]                 Dictionnary of kdes
    - mean_sp [dictionnary]                Dictionnary of mean value
    - max_val_dist [float]                 KDE maximal value among species
    - gx0y0                                Cartesian grid between min and max values in each dimension
    - gx0                                  Cartesian grid between min and max values in first dimension
    """
    gx0, gy0 = np.mgrid[np.min(x["value"]):np.max(x["value"]):128j, np.min(y["value"]):np.max(y["value"]):128j]
    gx0y0 = np.vstack((gx0.ravel(), gy0.ravel())) # shape is (128, 128, 2)
    max_val_dist = 0.0
    percentile_val_sp={}
    kde_sp={}
    mean_sp = {}
    for ie,hue in enumerate(x["Species"].unique()):
        db = x[x["Species"] == hue]
        d = y[y["Species"] == hue]
        xy = np.vstack((db["value"],d["value"]))
        kde_sp[hue] = stats.gaussian_kde(xy)        
        max_val_dist=np.max([max_val_dist,np.max(kde_sp[hue](gx0y0))])
        kde_xy=kde_sp[hue](xy)
        percentile_val_sp[hue] = {p:np.percentile(kde_xy,p) for p in percentiles}
        mean_sp[hue]=np.mean(kde_xy)
    return percentile_val_sp, mean_sp, kde_sp, max_val_dist, gx0y0, gx0, gy0


def PlotIsolineTernary(contour_2D,tax,scale=60, labels=[r'Ground truth']):
    """
    Plot 2D isolines in a ternary plot
    
    Input:
    - contour_2D [contour matplotlib object]        Contours have been computed on a 2D map with the matplotlib function 'contour'. Contour_2D is returned by this function.
    - tax [ternary plot axis object]                ternary plot axis object : tax is produced by the ternary plot package and wraps the matplotlib object
    - scale [float]                                 Scale of the ternary plot
    
    Output:
    -tax [ternary plot axis object]                 ternary plot axis with the isolines.
    """
    for ii, contour in enumerate(contour_2D.allsegs):
        for jj, seg in enumerate(contour):
            l2 = seg[:,1]*2/np.sqrt(3)
            l3 = (seg[:,0]*2-l2)/2
            l2 *= scale
            l3 *= scale
            l2bis = list()
            l3bis = list()
            l1bis = list()
            for lll,l in enumerate(l2):
                if (l<scale)and(l3[lll]<scale)and(l>0)and(l3[lll]>0)and(scale-l-l3[lll]<scale)and(scale-l-l3[lll]>0):
                    l2bis.append(l)
                    l3bis.append(l3[lll])
                    l1bis.append(scale-l-l3[lll])
            l2 = l2bis
            l3 = l3bis
            l1 = l1bis
            ll = [(l3[i],l2[i],l1[i]) for i in range(len(l2))]
            if (ii==0):
                tax.plot(ll, color='C0', label=labels[0], lw=1.5)
            else:
                tax.plot(ll, color='C0', lw=1.5)
    tax.boundary(linewidth=2.0)
    tax.right_corner_label(r'$A(\eta)$', fontsize=20)
    tax.top_corner_label(r'$A(\nabla b)$',fontsize=20)
    tax.left_corner_label(r'$A(b)$',fontsize=20)
    tax.get_axes().axis('off')
    tax.clear_matplotlib_ticks()
    tax.get_axes().set_aspect('equal')
    return tax




def PlotDescriptors(data_visu, data_visu_control=None, OneD_distribution_list=[r'$||A||$',r'$||V||$','dist','disp','Area'], TwoD_distribution_list=[[r'$||\nabla b||$','A'],['b','V'],['dist','disp']], quantiles=[5,50,95],figsize=(6,3),xlim=False):
    """
    Plot the species trajectory descriptors (same plots than in the publication)
    
    Input:
    - data_visu [list]          list of datasets
    
    Output:
    - fig [matplotlib object]   the matplotlib figure
    """
    shift = 0.1
    fig = plt.figure(constrained_layout=True,figsize=figsize)     
#    gs = fig.add_gridspec(nrows=8, ncols=5*len(OneD_distribution_list), wspace=0.4, hspace=0.1)
        
#    gs = fig.add_gridspec(nraws,ncols)
    gs = fig.add_gridspec(nrows=2,ncols=1,height_ratios=[0.4, 0.6])
    
    gs0= gs[0].subgridspec(1,len(OneD_distribution_list))
#    gs02=gs0[-1].subgridspec(1,1)
    gs1 = gs[1].subgridspec(1,len(TwoD_distribution_list))
    axs={0:{},1:{}}

    for j,descriptor in enumerate(OneD_distribution_list):
        axs[0][j] = fig.add_subplot(gs0[j])
        axs[0][j].set_xlabel(descriptor)
        axs[0][j].yaxis.set_visible(False)
        x=data_visu[descriptor]        
        percentile_val_sp, mean_sp, kde_sp, max_val_dist,x0,number = ComputePercentile1D(x)
        if data_visu_control is not None:
            x_control = data_visu_control[descriptor]        
            percentile_val_sp_control, mean_sp_control, kde_sp_control, max_val_dist_control,x0_control,number_control = ComputePercentile1D(x_control)
        nb_hue=len(x["Species"].unique())-1     
        print(5*'*'+descriptor)
        for ie,hue in enumerate(x["Species"].unique()):
            kde_dys = kde_sp[hue](x0)
            if data_visu_control is not None:
                kde_dys_control = kde_sp_control[hue](x0)
            print('number of trajectories', hue,number[hue])
            print('mean',hue,mean_sp[hue])
            print('mean control',hue,mean_sp_control[hue])
            print('percentile',hue,percentile_val_sp[hue])
            print('percentile control',hue,percentile_val_sp_control[hue])            
            print('wideness',hue,percentile_val_sp[hue][95]-percentile_val_sp[hue][5])
            #axs[0][j].text(0, (nb_hue-ie)*max_val_dist+shift + 0.5, 'n='+str(number[hue]),ha='left',va='center',color='C'+str(ie))
            axs[0][j].plot(x0,(nb_hue-ie)*max_val_dist+shift + kde_dys, label=hue, color='C'+str(ie))
            if data_visu_control is not None:
                axs[0][j].plot(x0,(nb_hue-ie)*max_val_dist+shift + kde_dys_control,ls=':',color='C'+str(ie))            
            axs[0][j].fill_between(x0,(nb_hue-ie)*max_val_dist+shift, (nb_hue-ie)*max_val_dist+shift+ kde_dys, color='C'+str(ie),            
                                alpha=0.4, lw=0.5) 

        hmax = axs[0][j].get_ylim()[1]
        hmin = axs[0][j].get_ylim()[0]
        min_val=np.inf
        max_val=-np.inf
        for ie,hue in enumerate(x["Species"].unique()):
            kde_dys = kde_sp[hue](x0)
            for q in quantiles:
                axs[0][j].axvline(x=percentile_val_sp[hue][q],ymin=((nb_hue-ie)*max_val_dist+shift - hmin)/(hmax-hmin), 
                        ymax=((nb_hue-ie)*max_val_dist+shift+ np.max(kde_dys) - hmin)/(hmax-hmin),
                        linewidth=1,zorder=2,color='C'+str(ie))
                min_val=min(min_val,percentile_val_sp[hue][q])
                max_val=max(max_val,percentile_val_sp[hue][q])
            axs[0][j].axvline(x=mean_sp[hue],ymin=((nb_hue-ie)*max_val_dist+shift - hmin)/(hmax-hmin), 
                        ymax=((nb_hue-ie)*max_val_dist+shift+ np.max(kde_dys) - hmin)/(hmax-hmin),
                        linestyle='--', linewidth=1,zorder=2,color='C'+str(ie))
        if xlim:
            axs[0][j].set_xlim(0.9*min_val,1.1*max_val)


    for j,descriptor in enumerate(TwoD_distribution_list):
        axs[1][j] = fig.add_subplot(gs1[j])
        x = data_visu[descriptor[0]] #9
        y = data_visu[descriptor[1]]
        percentile_val_sp, mean_sp, kde_sp, max_val_dist, gx0y0, gx0, gy0  = ComputePercentile2D(x,y)
        axs[1][j].set_xlabel(descriptor[0])
        if descriptor[1] in ['A','V']:
            axs[1][j].set_ylabel(r'||'+descriptor[1]+'||')
        else:
            axs[1][j].set_ylabel(descriptor[1])
        leg = {}
        for ie,hue in enumerate(x["Species"].unique()):
            kde_dys = np.reshape(kde_sp[hue](gx0y0).T,gx0.shape)
            cntr = axs[1][j].contour(gx0, gy0, kde_dys, levels=[percentile_val_sp[hue][5],percentile_val_sp[hue][50], percentile_val_sp[hue][95]], colors='C'+str(ie))
            leg[ie],_ = cntr.legend_elements()
#        axs[1][j].set_aspect(1.0/axs[1][j].get_data_ratio(), adjustable='box')
        if j==len(TwoD_distribution_list)-1:
            label_l = [r'$'+hue[0].upper()+'.'+hue[1:]+'$' if hue[0].lower()=='b' else hue for hue in x['Species'].unique()]
            axs[1][j].legend([leg[ie][0] for ie,hue in enumerate(x["Species"].unique())], label_l)
        print("xlim",np.quantile(x["value"],0.05),np.quantile(x["value"],0.99))
        print("ylim",np.quantile(y["value"],0.05),np.quantile(y["value"],0.99))
        if xlim:    
            if j==2:
                axs[1][j].set_xlim(0.0,np.quantile(x["value"],0.95))
                axs[1][j].set_ylim(0.0,np.quantile(x["value"],0.95))
            else:   
                axs[1][j].set_xlim(np.quantile(x["value"],0.005),np.quantile(x["value"],0.995))
                axs[1][j].set_ylim(np.quantile(x["value"],0.015),np.quantile(y["value"],0.985))
#        plt.gcf().subplots_adjust(wspace = 0.1, hspace = 0.2)
    return fig   

