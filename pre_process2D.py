#!python
"""
This file contains different pre-processing routines.
It is mainly used to generate the input data for the swimmer model and the STAN inference

Contents:
    - HDF5_Read:                read the data from a HDF5 file
    - get_Biofilm_Image:    wraps the acquisition of biofilm images and gradients
    - read_stan_file:        read the STAN output from the STAN sampling
    - read_data_file:        read the STAN input data from the DATA reading
    - read_simu_file:        read the STAN input data from the SIMU reading    
    - read_data:            read the STAN input data from the DATA result directory
"""

## Load library
import sys
import h5py
import argparse
import imageio as imio
import numpy as np
import gzip, pickle



def HDF5_Read(output_file,dset):
    """
    read the data from a HDF5 file
    Input:
        - output_file [str]:        name of the output file
         - dset []:
    Output:
        - data array
    """
    h5f = h5py.File(output_file,"r")
    return np.array(h5f.get(dset))




def get_Biofilm_Image(Data_dimensions,Data_files,OutsideColor=-1):
    """
    wraps the acquisition of biofilm images and gradients
    Input:
        - Data_dimensions:  dictionnary of image dimensions
        - Data_files:       dictionnary of image files
        - OutsideColor:     colorcode for the outside of the plate   
    Output:
        - bact [array]:         Temporal stack of bacterial density in biofilm
        - gradb_x [array]:      Temporal stack of biofilm density gradient (x direction)
        - gradb_y [array]:      Temporal stack of biofilm density gradient (y direction)
        - is_in_plate [array]:  Temporal stack of image mask : allows to avoid pixels outside the domain (for example outside the plate).
    """
    m = Data_dimensions['m']
    n = Data_dimensions['n']
    Ncycle = Data_dimensions['Ncycle']        
    #----2D+T reconstruction of biofilm images----#
    Image2D = np.zeros((Ncycle+1,m,n))             # original image (mock biofilm)
    bact = np.zeros((Ncycle+1,m,n))                # biofilm bacteria density maps
    gradb_x = np.zeros((Ncycle+1,m,n))             # biofilm bacteria density gradient maps (x direction)
    gradb_y = np.zeros((Ncycle+1,m,n))             # biofilm bacteria density gradient maps (y direction)

    for k in range(Ncycle+1):
        Image2D[k,:,:] = HDF5_Read(Data_files['biofilm_gray_scale'],'biofilm_gray_scale')
        gradb_x[k,:,:] = HDF5_Read(Data_files['biofilm_grad_bact_x'],'biofilm_grad_bact_x')
        gradb_y[k,:,:] = HDF5_Read(Data_files['biofilm_grad_bact_y'],'biofilm_grad_bact_y')
                           
    is_in_plate = np.ones((Ncycle+1,m,n))
    is_in_plate[Image2D[:,:,:]==OutsideColor]=0    # define which pixel of the image are outside the plate (colorcoded in the images)
    is_in_plate=is_in_plate[:].reshape((Ncycle+1,m,n))
    bact[:,:,:] = Image2D[:,:,:]
    return bact, gradb_x, gradb_y, is_in_plate




def read_stan_file(stanfile):
    """
    read the STAN output from the STAN sampling

    Input:
         - stanfile [str]:                name of the STAN file to read
    Output:
        - parameter [vect]:                STAN sampling output
    """
    import gzip, pickle

    print("STAN file: ",stanfile)
    with gzip.open(stanfile, 'rb') as fp:
        parameter = pickle.load(fp)

    return parameter



def read_data_file(datafile):
    """
    read the STAN input data from the DATA reading
    
    Input:
         - datafile [str]:                name of the DATA file to read
    Output:
        - Y [vect]:                    vector associated to the DATA file
    """
    import gzip, pickle

    print("DATA file: ",datafile)
    with gzip.open(datafile, 'rb') as fp:
        Y = pickle.load(fp)

    return Y



def read_simu_file(simufile):
    """
    read the STAN input data from the SIMU reading
    
    Input:
         - datafile [str]:                name of the SIMU file to read
    Output:
        - Y [vect]:                    vector associated to the SIMU file
    """

    import gzip, pickle

    print("SIMU file: ",simufile)
    with gzip.open(simufile, 'rb') as fp:
        Y = pickle.load(fp)

    return Y




def get_data(Param,data,bact,gradb_x,gradb_y):
    """
    Extract and reformate the data needed for Stan inference

    Input:
        - Param [dictionnary]:                          Simulation parameter dictionnary
        - data [data class]:                            data class
        - bact [array]:         Temporal stack of bacterial density in biofilm
        - gradb_x [array]:      Temporal stack of biofilm density gradient (x direction)
        - gradb_y [array]:      Temporal stack of biofilm density gradient (y direction)        

    Output:
        - YVx [vect]:                                   velocity (x-direction)
        - YVy [vect]:                                   velocity (y-direction)
        - YB [vect]:                                    biofilm density
        - YgradBx [vect]:                               gradient of the biofilm density (x-direction)
        - YgradBy [vect]:                               gradient of the biofilm density (y-direction)
        - YAx [vect]:                                   acceleration (x-direction)
        - YAy [vect]:                                   acceleration (y-direction)
        - Yx [vect]:                                    position (x-direction)
        - Yy [vect]:                                    position (y-direction)
        - YindexTime [vect]:                            index array (CSR-like structure)
        - YindexFirst [vect]:                           index array of the first elements (CSR-like structure), corresponding to Y dimensions
        - YindexFirstV [vect]:                          index array of the first elements (CSR-like structure), corresponding to V dimensions
        - YindexFirstA [vect]:                          index array of the first elements (CSR-like structure), corresponding to A dimensions
        - truePositionsNb:                              Number of positions
    """    
    #----Stan Inference----#
    Param['Nt'] = (Param['Ncycle']+1)*data.swimmer_Nb #np.sum(data.swimmer_totalTime)
    YindexTimelist = list()  # np.zeros(Nt,int)
    Yxlist = list() # = np.zeros(Nt)
    Yylist = list() #np.zeros(Nt)
    YBlist = list() #np.zeros(Nt)
    YgradBxlist = list() # np.zeros(Nt)
    YgradBylist = list() # np.zeros(Nt)
    YVxlist = list() # np.zeros(Ncycle*data.swimmer_Nb)
    YVylist = list() # np.zeros(Ncycle*data.swimmer_Nb)
    YAxlist = list() # np.zeros((Ncycle-1)*data.swimmer_Nb)
    YAylist = list() # np.zeros((Ncycle-1)*data.swimmer_Nb)
    vel0x = np.zeros(data.swimmer_Nb)
    vel0y = np.zeros(data.swimmer_Nb)
    YindexFirst = np.zeros(data.swimmer_Nb+1,int)
    YindexFirstV = np.zeros(data.swimmer_Nb+1,int)
    YindexFirstA = np.zeros(data.swimmer_Nb+1,int)
    count = 0
    sdo = 0.1
    for s in range(data.swimmer_Nb):
        vel0x[s] = data.swimmer_Vx[s][0] #+np.random.normal(0, sqrt(2)*sdo/dt, 1)
        vel0y[s] = data.swimmer_Vy[s][0] #+np.random.normal(0, sqrt(2)*sdo/dt, 1)
        for t in range(data.swimmer_totalTime[s]):
            Yxlist.append(data.swimmer_posX[s][t] - data.length_x_inf)
            Yylist.append(data.swimmer_posY[s][t] - data.length_y_inf)
            indexI, indexJ = data.GetIndex([data.swimmer_posX[s][t], data.swimmer_posY[s][t]])
            YBlist.append(bact[data.swimmer_indexTime[s][t], indexI, indexJ])
            YgradBxlist.append(gradb_x[data.swimmer_indexTime[s][t], indexI, indexJ])
            YgradBylist.append(gradb_y[data.swimmer_indexTime[s][t], indexI, indexJ])
            YindexTimelist.append(int(data.swimmer_indexTime[s][t]+1))
        YindexFirst[s] = int(count+1)
        count = count + data.swimmer_totalTime[s]
    YindexFirst[-1] = int(count+1)
    count = 0
    for s in range(data.swimmer_Nb):
        for t in range(len(data.swimmer_Ax[s])):
            YAxlist.append(data.swimmer_Ax[s][t])
            YAylist.append(data.swimmer_Ay[s][t])
        YindexFirstA[s] = int(count+1)
        count = count + len(data.swimmer_Ax[s])
    YindexFirstA[-1] = int(count+1)
    count = 0
    for s in range(data.swimmer_Nb):
        for t in range(len(data.swimmer_Vx[s])):
            YVxlist.append(data.swimmer_Vx[s][t])
            YVylist.append(data.swimmer_Vy[s][t])
        YindexFirstV[s] = int(count+1)
        count = count + len(data.swimmer_Vx[s])
    YindexFirstV[-1] = int(count+1)

    YindexTime = np.array(YindexTimelist,int)
    Yx = np.array(Yxlist)
    Yy = np.array(Yylist)
    YB = np.array(YBlist)
    YgradBx = np.array(YgradBxlist)
    YgradBy = np.array(YgradBylist)
    YVx = np.array(YVxlist)
    YVy = np.array(YVylist)
    YAx = np.array(YAxlist)
    YAy = np.array(YAylist)
    truePositionsNb = YindexFirst[-1]-1 #int(np.sum(isActive)/2)
    return YVx,YVy,YB,YgradBx,YgradBy,YAx,YAy,Yx,Yy,YindexTime,YindexFirst,YindexFirstV,YindexFirstA,truePositionsNb





def write_data(datadir,YVx,YVy,YB,YgradBx,YgradBy,YAx,YAy,Yx,Yy,YindexTime,YindexFirst,YindexFirstV,YindexFirstA,data,data_basis='DATA'):
    """
    read the STAN input data from the DATA result directory
    Input:
         - datadir [str]:                name of the DATA result directory
        - YVx [vect]:                                   velocity (x-direction)
        - YVy [vect]:                                   velocity (y-direction)
        - YB [vect]:                                    biofilm density
        - YgradBx [vect]:                               gradient of the biofilm density (x-direction)
        - YgradBy [vect]:                               gradient of the biofilm density (y-direction)
        - YAx [vect]:                                   acceleration (x-direction)
        - YAy [vect]:                                   acceleration (y-direction)
        - Yx [vect]:                                    position (x-direction)
        - Yy [vect]:                                    position (y-direction)
        - YindexTime [vect]:                            index array (CSR-like structure)
        - YindexFirst [vect]:                           index array of the first elements (CSR-like structure), corresponding to Y dimensions
        - YindexFirstV [vect]:                          index array of the first elements (CSR-like structure), corresponding to V dimensions
        - YindexFirstA [vect]:                          index array of the first elements (CSR-like structure), corresponding to A dimensions
        - data [data class]:                            data class
        - data basis [str]:                             basis of the file names
    Output:
        void
    """
    with gzip.open(datadir+'/'+data_basis+'_YVx.dat', 'wb') as fp:
        pickle.dump(YVx,fp)
    with gzip.open(datadir+'/'+data_basis+'_YVy.dat', 'wb') as fp:
        pickle.dump(YVy,fp)
    with gzip.open(datadir+'/'+data_basis+'_YB.dat', 'wb') as fp:
        pickle.dump(YB,fp)
    with gzip.open(datadir+'/'+data_basis+'_YgradBx.dat', 'wb') as fp:
        pickle.dump(YgradBx,fp)
    with gzip.open(datadir+'/'+data_basis+'_YgradBy.dat', 'wb') as fp:
        pickle.dump(YgradBy,fp)
    with gzip.open(datadir+'/'+data_basis+'_YAx.dat', 'wb') as fp:
        pickle.dump(YAx,fp)
    with gzip.open(datadir+'/'+data_basis+'_YAy.dat', 'wb') as fp:
        pickle.dump(YAy,fp)
    with gzip.open(datadir+'/'+data_basis+'_Yx.dat', 'wb') as fp:
        pickle.dump(Yx,fp)
    with gzip.open(datadir+'/'+data_basis+'_Yy.dat', 'wb') as fp:
        pickle.dump(Yy,fp)
    with gzip.open(datadir+'/'+data_basis+'_YindexTime.dat', 'wb') as fp:
        pickle.dump(YindexTime,fp)
    with gzip.open(datadir+'/'+data_basis+'_YindexFirst.dat', 'wb') as fp:
        pickle.dump(YindexFirst,fp)
    with gzip.open(datadir+'/'+data_basis+'_YindexFirstV.dat', 'wb') as fp:
        pickle.dump(YindexFirstV,fp)
    with gzip.open(datadir+'/'+data_basis+'_YindexFirstA.dat', 'wb') as fp:
        pickle.dump(YindexFirstA,fp)
    with gzip.open(datadir+'/'+data_basis+'_data.dat', 'wb') as fp:
        pickle.dump(data,fp)







def read_data(datadir,data_basis='DATA', data_list=['YVx', 'YVy', 'YB', 'YgradBx', 'YgradBy', 'YAx', 'YAy', 'Yx', 'Yy', 'YindexTime', 'YindexFirst', 'YindexFirstV', 'YindexFirstA','data']
):
    """
    read the STAN input data from the DATA result directory
    Input:
         - datadir [str]:                               name of the DATA result directory
         - data_basis [str]                             basis of the file names
    Output:
        - YVx [vect]:                                   velocity (x-direction)
        - YVy [vect]:                                   velocity (y-direction)
        - YB [vect]:                                    biofilm density
        - YgradBx [vect]:                               gradient of the biofilm density (x-direction)
        - YgradBy [vect]:                               gradient of the biofilm density (y-direction)
        - YAx [vect]:                                   acceleration (x-direction)
        - YAy [vect]:                                   acceleration (y-direction)
        - Yx [vect]:                                    position (x-direction)
        - Yy [vect]:                                    position (y-direction)
        - YindexTime [vect]:                            index array (CSR-like structure)
        - YindexFirst [vect]:                           index array of the first elements (CSR-like structure), corresponding to Y dimensions
        - YindexFirstV [vect]:                          index array of the first elements (CSR-like structure), corresponding to V dimensions
        - YindexFirstA [vect]:                          index array of the first elements (CSR-like structure), corresponding to A dimensions
        - data [data class]:                            data class
    """
    output = {}
    for key in data_list:
        output[key] = read_data_file(datadir+'/'+data_basis+'_'+key+'.dat') 

    return output






