#!/usr/bin/python
"""
This file contains the classes related to the random walk of the swimmers.
It is mainly used for updating the swimmers positions and velocities.

Contents:
    - SwimmerDomain class:                        handle the domain definition for the swimmers
    - SwimmerCaracteristic class:                handle the data structures to keep track of the trajectories
                                                Do not contain the random walk or trail model characteristics
    - Brownian_Langevin_Interaction_2 class:    Langevin with interaction with the media
    - Brownian class:                           Wrapper of the random walk model.
    - Trajectory_Init:                          Wrapper that instanciates a list of swimmers
"""

## Load library
import numpy as np



class SwimmerDomain(object):
    """
            handle the domain definition for the swimmers
    Contains:
        variables:
            - corner_inf [array]:            inferior corner of the 2D domain
            - corner_sup [array]:            superior corner of the 2D domain
            - length_x_inf [float]:            x-origin
            - length_x_sup [float]:            x-boundary
            - length_y_inf [float]:            y-origin
            - length_y_sup [float]:            y-boundary
            - sub_domain_origin [array]:    origin of the subdomain (owned by the current process)
            - N [int]:                        number of swimmer cycles by diffusion cycle (different time scale for accuracy issues)
            - steps_diff [int]:                number of cycles for diffusion
            - steps_swimmer [int]:            number of cycles for swimmers
            - domain_grid [array]:            number of cells in each direction
            - m [int]:                        x-index (number of pixels)
            - n [int]:                        y-index (number of pixels)
            - dt [float]:                    diffusion time step
            - dts [float]:                    swimmer time steps
            - dx [float]:                    spatial step (x-direction)
            - dy [floa]:                    spatial step (y-direction)
            - b [array]:                    biofilm density (image stack stored as [Ncycle x m x n])
            - gradbX [array]:                gradient of the biofilm density (x-direction - [Ncycle x m x n])
            - gradbY [array]:                gradient of the biofilm density (y-direction - [Ncycle x m x n])
            - color [array]:                color map to define the inside and outside of the computation domain (outside the plate)
            - GradDist_x [array]:            gradient of the distance function towards the exterior of the computation domain (x-direction)
            - GradDist_y [array]:            gradient of the distance function towards the exterior of the computation domain (y-direction)
            - color_outside_domain [int]:    color code to define the outside of the domain
            - dim [int]:                    number of spatial dimensions

        methods:
            - init:                            definition and initialisation of all class variables
            - polar_to_cart:                convert polar coordinates to cartesian coordinates
            - GetBiofilmColorCode:            define in a 0/1 basis the subdomain inside the biofilm. If, e.g., the inferior basis of the plate is displayed on the microscopy images, we can provide an Image with the color 1 outside the plate, and 0 inside the biofilm. The colorcode is coded with the variable self.color_outside_domain. The arrays GradDist_x,GradDist_y store the gradient of the distance function from the outside of the biofilm
            - GetBiofilm:                    get the biofilm densities
            - IsNotInDomain:                determine if a point is not in the domain
    """


    def __init__(self,dt,final_time,swimmer_N,domain_grid,global_domain, sub_domain_origin):
        """
        definition and initialisation of all class variables
        Input:
            - dt [float]:                    time step
            - final_time [float]:            final time of the simulation
            - swimmer_N [int]:                number of time steps for swimmer during a time step for diffusion (different time scale for accuracy issues)
            - domain_grid [array]:            number of cells in each direction
            - global_domain [array]:        domain diagonal corner coordinates
            - sub_domain_origin [array]:    origin of the subdomain (owned by the current process)
        Output:
            void
        """
        ######domain  dimension
        self.corner_inf = global_domain[0]
        self.corner_sup = global_domain[1]
        self.length_x_inf=global_domain[0][0]
        self.length_x_sup=global_domain[1][0]
        self.length_y_inf=global_domain[0][1]
        self.length_y_sup=global_domain[1][1]
        self.sub_domain_origin = sub_domain_origin     #origin of the subdomain

        ######time and spatial steps, time and spatial grid lengths
        #time grid
        self.N=swimmer_N                # number of swimmer cycles by diffusion cycle
        self.steps_diff = int((final_time)/dt)        #number of cycles for diffusion
        self.steps_swimmer=int(swimmer_N*(final_time)/dt)        #number of cycles for swimmers
        #space grid
        self.domain_grid=domain_grid
        self.m = domain_grid[0]
        self.n = domain_grid[1]
        #time step
        self.dt = dt                    #diffusion time step
        self.dts=self.dt/self.N                #swimmer time steps
        #space steps
        self.dx=(self.length_x_sup-self.length_x_inf)/float(self.m)
        self.dy=(self.length_y_sup-self.length_y_inf)/float(self.n)

        ######biofilm in domain
        self.b = None                    #gray scale densities for biofilm
        self.gradb_x = None
        self.gradb_y = None
        self.color = None                #color map to define the inside and outside of the computation domain (outside the plate)
        self.GradDist_x = None                #gradient of the distance function towards the exterior of the computation domain
        self.GradDist_y = None
        self.color_outside_domain=1.0            #color code to define the outside of the domain
        self.dim=2



    def polar_to_cart(self,vect_polar): #vect_polar on the form r, theta
        """
        convert polar coordinates to cartesian coordinates
        Input:
            - vect_polar [array]:            polar coordinates
        Output:
            - ptsnew [array]:                cartesian coordinates        
        """
        ptsnew=np.zeros((2))
        ptsnew[0]=vect_polar[0]*np.cos(vect_polar[1])
        ptsnew[1]=vect_polar[0]*np.sin(vect_polar[1])
        return ptsnew


    def GetBiofilmColorCode(self,IsBiofilm, IsBiofilm_grad_x=None, IsBiofilm_grad_y=None):
        """
        define in a 0/1 basis the subdomain inside the biofilm
        If, e.g., the inferior basis of the plate is displayed on the microscopy images, we can provide an Image with the color 1 outside the plate, and 0 inside the biofilm
        The colorcode is coded with the variable self.color_outside_domain. The arrays GradDist_x,GradDist_y store the gradient of the distance function from the outside of the biofilm
        Input:
            - IsBiofilm [array]:            color map to define the inside and outside of the computation domain (outside the plate)
            - IsBiofilm_grad_x [array]:        gradient of the distance function towards the exterior of the computation domain (x-direction)
            - IsBiofilm_grad_y [array]:        gradient of the distance function towards the exterior of the computation domain (y-direction)
        Output:
            void
        """
        self.color = IsBiofilm        
        self.GradDist_x = IsBiofilm_grad_x
        self.GradDist_y = IsBiofilm_grad_y


    def GetBiofilm(self,b,Gradb_x,Gradb_y):
        """
        get the biofilm densities
        Input:
            - b [array]:                    biofilm density (image stack stored as [Ncycle x m x n])
            - Gradb_x [array]:                gradient of the biofilm density (x-direction - [Ncycle x m x n])
            - Gradb_y [array]:                gradient of the biofilm density (y-direction - [Ncycle x m x n])
        Output:
            void
        """
        self.b = b
        self.gradb_x = Gradb_x
        self.gradb_y = Gradb_y


    def IsNotInDomain(self,pts):
        """
        determine if a point is not in the domain
        Calls:
            - SwimmerCaracteristic.GetIndex
        Input:
           - pts [array]:                    considered position
        Output:
            - str: 'Out_Of_Domain' or 'Out_Of_Plate' or 'In'
        """
        if np.any(pts<[self.corner_inf[0]-self.dx/2,self.corner_inf[1]-self.dy/2]) or np.any(pts>[self.corner_sup[0]+self.dx/2,self.corner_sup[1]+self.dy/2]):
            return 'Out_Of_Domain'
        else:
            if self.color is not None:
                i,j = self.GetIndex(pts)
                if self.color[i,j]==0:
                    print("OUT OF PLATE")
                    return 'Out_Of_Plate'
        return 'In'




class SwimmerCaracteristic(object):
    """
            handle the data structures to keep track of the trajectories. Do not contain the random walk or trail model characteristics
    Contains:
        variables:
            - time [float]:                                current time of the particle
            - swimmer_ID [array]:                        current swimmer ID (can change during the simulation if the particule goes outside the domain : new particle is created and replace the current particle from this time
            - curr_swimmer_ID [int]:                    ID of the current particle
            - pos [array]:                                particle positions
            - vel [array]:                                particle velocities
            - pore_off [array]:                            particle pores
            - radius [array]:                            particle radius
            - pore_radius_along_trajectory [array]:        particle pore radius along trajectory
            - initV [array]:                            initial velocity

        methods:
            - init:                                        definition and initialisation of all class variables
            - GetInitialPosition:                        sample the initial position in the biofilm domain
            - GetIndex:                                    return the index of a point in the underlying grid
            - solveNext:                                advance the swimmer to the next time step, by updating its position and velocity
            - solveNextNondim:                            advance the swimmer to the next time step, by updating its position and velocity [non-dimensionalized]
            - advanceANondim:                            advance the swimmer to the next time step, by updating its position and velocity, from the input acceleration [non-dimensionalized]
    """



    def __init__(self):
        """    
        definition and initialisation of all class variables
        Input:
            void
        Output:
            void
        """
        #particle time
        self.time = np.zeros((self.steps_diff+1))             #current time of the particle

        #particle ID
        self.swimmer_ID = np.zeros((self.steps_diff+1),int)        #current swimmer ID (can change during the simulation if the particule goes outside the domain : new particle is created and replace the current particle from this time
        self.curr_swimmer_ID=0                    #ID of the current particle

        #particle position
        self.pos = np.zeros((self.steps_diff+1,2))
        self.vel = np.zeros((self.steps_diff+1,2))

        #particle tail
        self.pore_off = np.ones((self.steps_diff+1,1))
        self.radius=np.zeros(self.steps_diff)
        self.pore_radius_along_trajectory=np.zeros((self.steps_diff, self.steps_diff))

        # initial speed
        self.initV = np.random.multivariate_normal([0,0], 1*np.eye(2))#np.zeros(3)



    def GetInitialPosition(self,Yinit=np.array([np.inf,np.inf]),Vinit=np.array([np.inf,np.inf])):
        """
        sample the initial position in the biofilm domain
        Calls:
            - SwimmerDomain.IsNotInDomain
        Input:
            - Yinit [array]:                initial positions, optional
            - Vinit [array]:                initial velocities, optional
        Output:
            void
        """
        if Yinit[Yinit==np.inf].shape[0]==2:
            init_pos = np.array([np.random.uniform(self.length_x_inf,self.length_x_sup),np.random.uniform(self.length_y_inf,self.length_y_sup)])
            while self.IsNotInDomain(init_pos)=='Out_Of_Plate':
                init_pos = np.array([np.random.uniform(self.length_x_inf,self.length_x_sup),np.random.uniform(self.length_y_inf,self.length_y_sup)])
            self.pos[0, :] = init_pos[:] #/0.45
        else:
            self.pos[0,0:self.dim] = Yinit[:]
        if not Vinit[Vinit==np.inf].shape[0]==2:
            self.initV[0:self.dim] = Vinit[:]
        self.vel[0,0:self.dim] = self.initV[0:self.dim]



    def GetIndex(self,pts):
        """
        return the index of a point in the underlying grid
        Input:
            - pts [array]:                    considered point
        Output:
            - i [int]:                        corresponding index along the x-direction
            - j [int]:                        corresponding index along the y-direction
        """        
        i = min(max(0,int((pts[0]-self.sub_domain_origin[0]-self.dx/2)/self.dx)),self.m-1)
        j = min(max(0,int((pts[1]-self.sub_domain_origin[1]-self.dy/2)/self.dy)),self.n-1)
        return [i,j]


    def solveNext(self,i,origin_subdomain,b,gradb_x,gradb_y):
        """
        advance the swimmer to the next time step, by updating its position and velocity
        Calls:
            - Brownian.computeV
            - SwimmerDomain.IsNotInDomain
            - Brownian.CreateNewParticule
        Input:
            - i [int]:                        current time index
            - origin_subdomain [array]:        origin of the subdomain (owned by the current process)
            - b [array]:                    biofilm density (image stack stored as [Ncycle x m x n])
            - gradb_x [array]:                gradient of the biofilm density (x-direction - [Ncycle x m x n])
            - gradb_y [array]:                gradient of the biofilm density (y-direction - [Ncycle x m x n])
        Output:
            void
        """
        t = self.time[i]
        t = t + self.dt
        self.time[i+1] = t
        v = self.vel[i,:]
        j_init=i*self.N
        j_end=(i+1)*self.N
        pos_curr = self.pos[i,:]
        for j in range(j_init,j_end):
            eps = self.eps[j]
            vnew = self.computeV(v, pos_curr,eps)
            dxs_curr=vnew[:] * self.dts
            pos_new=pos_curr+dxs_curr
            if self.IsNotInDomain(pos_new)=='Out_Of_Domain':
                pos_curr,v_new = self.CreateNewParticule()
                vnew = v_new
                dxs_curr=vnew[:] * self.dts
                pos_new=pos_curr+dxs_curr
            while self.IsNotInDomain(pos_new)=='Out_Of_Plate':
                pos_curr,v_new = self.CreateNewParticule()
                vnew = v_new
                dxs_curr=vnew[:] * self.dts
                pos_new=pos_curr+dxs_curr
            v = vnew
            pos_curr=pos_new
        self.swimmer_ID[i+1] = self.curr_swimmer_ID
        for j in range(i):
            self.radius[j]=max(0, self.r-self.nu*(i-j)*self.dt)
            self.pore_radius_along_trajectory[i, j]=self.radius[j]
        self.pos[i+1,:] = pos_curr
        self.vel[i+1,:] = v


    def solveNextNondim(self,i,origin_subdomain,b,gradb_x,gradb_y,Xref,Vref):
        """
        advance the swimmer to the next time step, by updating its position and velocity [non-dimensionalized]
        Calls:
            - Brownian.computeVNondim
            - SwimmerDomain.IsNotInDomain
            - Brownian.CreateNewParticule
        Input:
            - i [int]:                        current time index
            - origin_subdomain [array]:        origin of the subdomain (owned by the current process)
            - b [array]:                    biofilm density (image stack stored as [Ncycle x m x n])
            - gradb_x [array]:                gradient of the biofilm density (x-direction - [Ncycle x m x n])
            - gradb_y [array]:                gradient of the biofilm density (y-direction - [Ncycle x m x n])
            - Xref [float]:                    length of reference
            - Vref [float]:                    speed of reference
        Output:
            void
        """
        t = self.time[i]
        t = t + self.dt
        self.time[i+1] = t
        v = self.vel[i,:]
        j_init=i*self.N
        j_end=(i+1)*self.N
        pos_curr = self.pos[i,:]
        for j in range(j_init,j_end):
            eps = self.eps[j]
            vnew = self.computeVNondim(v, pos_curr, eps, Xref, Vref)
            dxs_curr=vnew[:] * self.dts * Vref/Xref
            pos_new=pos_curr+dxs_curr
            if self.IsNotInDomain(pos_new)=='Out_Of_Domain':
                pos_curr,v_new = self.CreateNewParticule()
                vnew = v_new 
                dxs_curr=vnew[:] * self.dts * Vref/Xref
                pos_new=pos_curr+dxs_curr
            while self.IsNotInDomain(pos_new)=='Out_Of_Plate':
                pos_curr,v_new = self.CreateNewParticule()
                vnew = v_new #/Vref
                dxs_curr=vnew[:] * self.dts * Vref/Xref
                pos_new=pos_curr+dxs_curr
            v = vnew
            pos_curr=pos_new
        self.swimmer_ID[i+1] = self.curr_swimmer_ID
        for j in range(i):
            self.radius[j]=max(0, self.r-self.nu*(i-j)*self.dt)
            self.pore_radius_along_trajectory[i, j]=self.radius[j]
        self.pos[i+1,:] = pos_curr
        self.vel[i+1,:] = v


    def advanceANondim(self,i,origin_subdomain,b,gradb_x,gradb_y,Xref,Vref, YAx, YAy):
        """
        advance the swimmer to the next time step, by updating its position and velocity, from the input acceleration [non-dimensionalized]
        Calls:
            - Brownian.advanceVNondim
            - SwimmerDomain.IsNotInDomain
            - Brownian.CreateNewParticule
        Input:
            - i [int]:                        current time index
            - origin_subdomain [array]:        origin of the subdomain (owned by the current process)
            - b [array]:                    biofilm density (image stack stored as [Ncycle x m x n])
            - gradb_x [array]:                gradient of the biofilm density (x-direction - [Ncycle x m x n])
            - gradb_y [array]:                gradient of the biofilm density (y-direction - [Ncycle x m x n])
            - Xref [float]:                    length of reference
            - Vref [float]:                    speed of reference
            - YAx [float]:                    input accaleration [x-direction]
            - YAy [float]:                    input acceleration [y-direction]
        Output:
            void
        """
        t = self.time[i]
        t = t + self.dt
        self.time[i+1] = t
        v = self.vel[i,:]
        j_init=i*self.N
        j_end=(i+1)*self.N
        pos_curr = self.pos[i,:]
        for j in range(j_init,j_end):
            eps = self.eps[j]
            vnew = self.advanceVNondim(v, pos_curr, eps, Xref, Vref, YAx, YAy)
            dxs_curr=vnew[:] * self.dts * Vref/Xref
            pos_new=pos_curr+dxs_curr
            v = vnew
            pos_curr=pos_new
        self.swimmer_ID[i+1] = self.curr_swimmer_ID
        for j in range(i):
            self.radius[j]=max(0, self.r-self.nu*(i-j)*self.dt)
            self.pore_radius_along_trajectory[i, j]=self.radius[j]
        self.pos[i+1,:] = pos_curr
        self.vel[i+1,:] = v




class Brownian_Langevin_Interaction_2(SwimmerDomain,SwimmerCaracteristic):
    """
            Langevin with interaction with the media
    The equation is the following
    dv = (gamma(alpha(b)  - ||v||) v/||v|| + beta \grad(b)/||grad(b)|| )dt + eps dt
    * alpha is a constant parameter that represents the swimming force. This swimming force is supposed to be directed along the speed, and is regulated by the biofilm density : to a given biofilm density b, a nominal swimming speed alpha is defined. The model returns this nominal speed with a relaxation time gamma. The function b-> alpha(b) := v_0+b(v_1-v_0), where v_0 is the minimal speed (for maximal density b=0) and v_1 is the maximal speed (for b=1). The bacterial density is supposed to have been normalised before.
    * beta is a directional force that represent the ability of the swimmer to search for minimal resistance pathways. It searchs for the direction directed towards the less bacterial density, and apply a force with a magnitude beta. This force vanishes when grad(b) is too small. beta is smaller than alpha.
    * eps is a stochastic component with zero mean and variance 1.
    The initial position is sampled uniformly in the domain. The initial speed is normally distributed

    Contains:
        class inheritance:
            - SwimmerDomain
            - SwimmerCaracteristic

        variables:
            - time [float]:                                current time of the particle
            - beta [float]:                                mean acceleration due to the gradient term [model parameter]
            - gamma [float]:                            penalisation coefficient [model parameter]
            - eps [float]:                                stochastic coefficient [model parameter]
            - v0 [float]:                                minimal velocity in the biofilm [model parameter]
            - v1 [float]:                                maximal velocity in the biofilm [model parameter]
            - r [float]:                                swimmer radius
            - nu [float]:                                pore refilling speed

        methods:
            - init:                                        definition and initialisation of all class variables and parent classes
            - computeV:                                    update the velocity from the random walk model
            - computeVNondim:                                    update the velocity from the random walk model [non-dimensionalized]
            - advanceVNondim:                                    update the velocity from the random walk model, from the acceleration input [non-dimensionalized]
            - CreateNewParticule:                        define a new position for the particule on an available boundary of the domain
    """



    def __init__(self,dt,swimmer_N,runtime=100.0,trace_spec=(0.03,0.03),domain_spec=[[0,0],[1,1]],domain_grid=[256,256],sub_domain_origin=[0,0],random_walk_spec=(0.5,1,np.random.multivariate_normal([0,0], np.eye(2), 100),0.5,1.5)):
        """
        definition and initialisation of all class variables and parent classes
        Input:
            - dt [float]:                    time step
            - swimmer_N [float]:            number of time steps for swimmer during a time step for diffusion (different time scale for accuracy issues)
            - runtime [float]:                final time of the simulation, optional
            - trace_spec [array]:            pore features, optional
            - domain_spec [array]:            domain diagonal corner coordinates, optional
            - domain_grid [array]:            number of cells in each direction, optional
            - sub_domain_origin [array]:    origin of the subdomain (owned by the current process), optional
            - random_walk_spec [array]:        parameters of the random walk model, optional
        Output:
            void
        """
        SwimmerDomain.__init__(self,dt,runtime,swimmer_N,domain_grid,domain_spec, sub_domain_origin)
        SwimmerCaracteristic.__init__(self)
        #SwimmerCaracteristic.GetInitialPosition(self)
        self.beta = random_walk_spec[0]
        self.gamma = random_walk_spec[1]
        self.eps = random_walk_spec[2]
        self.v0 = random_walk_spec[3]
        self.v1 = random_walk_spec[4]

        self.r=trace_spec[0]                #swimmer radius
        self.nu=trace_spec[1]                #pore refilling speed



    def computeV(self, v,pos,eps,loc='int'):
        """
        update the velocity from the random walk model
        Calls:
            - SwimmerCaracteristic.GetIndex
            - SwimmerDomain.polar_to_cart
        Input:
            - v [array]:                    current velocity
            - pos [array]:                    current position
           - eps [float]:                    gaussian noise
            - loc [str]:                    label of the current position [interior or outside point], optional
        Output:
            - new velocity v+dv
        """
        tau = 1 #self.dts#*5
        i,j = self.GetIndex(pos)
        dv=np.zeros(2)
        gradb=np.asarray([self.gradb_x[i,j],self.gradb_y[i,j]])
        if np.linalg.norm(v)<1e-12: #intial time only
            angle=[np.random.uniform(0,2*np.pi)]
            vtmp = self.polar_to_cart([1,angle[0]])
        else:
            vtmp = v
        if loc=='int': #interior position
            dv[0:self.dim] = self.gamma*(self.v0 + self.b[i,j]*(self.v1-self.v0) -np.linalg.norm(vtmp[0:self.dim]))* vtmp[0:self.dim]/(np.linalg.norm(vtmp[0:self.dim])+1e-12)/tau*self.dts+ self.beta*gradb[0:self.dim]/(np.linalg.norm(gradb[0:self.dim])+1e-12) * self.dts + eps[0:self.dim] * self.dts#np.sqrt(self.dts)# with all terms.
        else: #boundary position
            raise NameError('Wrong position location in computeV')
        return v + dv

    def computeVNondim(self, v,pos,eps,Xref,Vref,loc='int'):
        """
        update the velocity from the random walk model [non-dimensionalized]
        Calls:
            - SwimmerCaracteristic.GetIndex
            - SwimmerDomain.polar_to_cart
        Input:
            - v [array]:                    current velocity
            - pos [array]:                    current position
            - eps [float]:                    gaussian noise
            - Xref [float]:                    length of reference
            - Vref [float]:                    lspeed of reference
            - loc [str]:                    label of the current position [interior or outside point], optional
        Output:
            - new velocity v+dv
        """
        tau = 1
        i,j = self.GetIndex(pos)
        dv=np.zeros(2)
        gradb=np.asarray([self.gradb_x[i,j],self.gradb_y[i,j]])
        if np.linalg.norm(v)<1e-12: #intial time only
            angle=[np.random.uniform(0,2*np.pi)]
            vtmp = self.polar_to_cart([1,angle[0]])
        else:
            vtmp = v
        if loc=='int': #interior position
            dv[0:self.dim] = self.gamma/Xref*Vref*(self.v0 + self.b[i,j]*(self.v1-self.v0) -np.linalg.norm(vtmp[0:self.dim]))* vtmp[0:self.dim]/(np.linalg.norm(vtmp[0:self.dim])+1e-12)/tau*self.dts+ self.beta*gradb[0:self.dim]/(np.linalg.norm(gradb[0:self.dim])+1e-12) * self.dts /Xref*Vref + eps[0:self.dim] * self.dts /Xref*Vref#np.sqrt(self.dts)# with all terms.
        else: #boundary position
            raise NameError('Wrong position location in computeV')
        return v + dv


    def advanceVNondim(self, v,pos,eps,Xref,Vref,YAx, YAy, loc='int'):
        """
        update the velocity from the random walk model, from the acceleration input [non-dimensionalized]
        Calls:
            - SwimmerCaracteristic.GetIndex
            - SwimmerDomain.polar_to_cart
        Input:
            - v [array]:                    current velocity
            - pos [array]:                    current position
            - eps [float]:                    gaussian noise
            - Xref [float]:                    length of reference
            - Vref [float]:                    lspeed of reference
            - YAx [float]:                    input accaleration [x-direction]
            - YAy [float]:                    input acceleration [y-direction]
            - loc [str]:                    label of the current position [interior or outside point], optional
        Output:
            - new velocity v+dv
        """
        dv=np.zeros(2)
        dv[0] = YAx*self.dts/Xref*Vref
        dv[1] = YAy*self.dts/Xref*Vref
        return v + dv


    def CreateNewParticule(self):
        """
        define a new position for the particule on an available boundary of the domain
        Calls:
            - SwimmerDomain.IsNotInDomain
            - SwimmerCaracteristic.GetIndex
            - SwimmerDomain.polar_to_cart
        Input:
            void
        Output:
            - array composed of the new position and the new velocity
        """
        rand = int(np.random.uniform()*4)
        if rand == 0: # bound x=self.length_x_inf
            New_pos = np.asarray([self.length_x_inf, np.random.uniform(self.length_y_inf,self.length_y_sup)])
            bot = self.length_y_inf
            top = self.length_y_sup
            while self.IsNotInDomain(New_pos) == 'Out_Of_Plate': #avoid taking a new position outside the plate
                New_pos = np.asarray([self.length_x_inf, np.random.uniform(bot,top)])
                if ((self.IsNotInDomain(New_pos) == 'Out_Of_Plate')and(not(self.IsNotInDomain([self.length_x_inf,bot])== 'Out_Of_Plate'))):
                    top = New_pos[1]
                else:
                    bot = New_pos[1]
            New_v= self.polar_to_cart([1,np.random.uniform(-np.pi/2,np.pi/2)])
        if rand == 1: # bound x=self.length_x_sup
            New_pos = np.asarray([self.length_x_sup, np.random.uniform(self.length_y_inf,self.length_y_sup)])
            bot = self.length_y_inf
            top = self.length_y_sup
            while self.IsNotInDomain(New_pos) == 'Out_Of_Plate': #avoid taking a new position outside the plate
                New_pos = np.asarray([self.length_x_sup, np.random.uniform(bot,top)])
                if ((self.IsNotInDomain(New_pos) == 'Out_Of_Plate')and(not(self.IsNotInDomain([self.length_x_sup,bot])== 'Out_Of_Plate'))):
                    top = New_pos[1]
                else:
                    bot = New_pos[1]
            New_v = self.polar_to_cart([1,np.random.uniform(np.pi/2,3*np.pi/2)])
        if rand == 2: # bound y=self.length_y_inf
            New_pos = np.asarray([np.random.uniform(self.length_x_inf,self.length_x_sup),self.length_y_inf])
            bot = self.length_x_inf
            top = self.length_x_sup
            while self.IsNotInDomain(New_pos) == 'Out_Of_Plate': #avoid taking a new position outside the plate
                New_pos = np.asarray([np.random.uniform(bot,top),self.length_y_inf])
                if ((self.IsNotInDomain(New_pos) == 'Out_Of_Plate')and(not(self.IsNotInDomain([bot,self.length_y_inf])== 'Out_Of_Plate'))):
                    top = New_pos[0]
                else:
                    bot = New_pos[0]
            New_v = self.polar_to_cart([1,np.random.uniform(0,np.pi)])
        if rand == 3: # bound y=self.length_y_sup
            New_pos = np.asarray([np.random.uniform(self.length_x_inf,self.length_x_sup),self.length_y_sup])
            bot = self.length_x_inf
            top = self.length_x_sup
            while self.IsNotInDomain(New_pos) == 'Out_Of_Plate': #avoid taking a new position outside the plate
                New_pos = np.asarray([np.random.uniform(bot,top),self.length_y_sup])
                if ((self.IsNotInDomain(New_pos) == 'Out_Of_Plate')and(not(self.IsNotInDomain([bot,self.length_y_sup])== 'Out_Of_Plate'))):
                    top = New_pos[0]
                else:
                    bot = New_pos[0]
            New_v = self.polar_to_cart([1,np.random.uniform(np.pi, 2*np.pi)])
        i,j = self.GetIndex(New_pos)
        New_v = (self.v0 + self.b[i,j]*(self.v1-self.v0))*New_v
        self.curr_swimmer_ID+=1
        return [np.asarray(New_pos),np.asarray(New_v)]






class Brownian:
    """
            model the Brownian motion
    The Brownian class is initialised to one specific random walk model.
    Several random walk models have been implemented:
        - Langevin_Interaction_2:                        Langevin with interaction with the media
        - RunAndTumble:                                    (To be updated)
        - Langevin:                                        (To be updated)
        - Langevin_Interaction:                            (To be updated)

    Contains:
        class inheritance:
            - RunAndTumble
            - Langevin
            - Langevin_Interaction
            - Langevin_Interaction_2

        methods:
            - init:                                        initialisation to one specific random walk model (parent class)
    """


    def __init__(self,dt,swimmer_N,runtime=100.0,trace_spec=(0.03,0.03),domain_spec=[[0,0],[1,1]],domain_grid=[256,256],sub_domain_origin=[0,0],random_walk_spec=(0.5,1,np.random.multivariate_normal([0,0], np.eye(2), 100),0.5,1.5),RandomWalkType='Langevin_Interaction_2'):
        """
        initialisation to one specific random walk model (parent class)
        Input:
            - dt [float]:                    time step
            - swimmer_N [float]:            number of time steps for swimmer during a time step for diffusion (different time scale for accuracy issues)
            - runtime [float]:                final time of the simulation, optional
            - trace_spec [array]:            pore features, optional
            - domain_spec [array]:            domain diagonal corner coordinates, optional
            - domain_grid [array]:            number of cells in each direction, optional
            - sub_domain_origin [array]:    origin of the subdomain (owned by the current process), optional
            - random_walk_spec [array]:        parameters of the random walk model, optional
            - RandomWalkType [str]:            name of the random walk model to consider, optional
        Output:
            void
        """
        if RandomWalkType=='Langevin_Interaction_2':##default
            self.trajectory=Brownian_Langevin_Interaction_2(dt,swimmer_N,runtime,trace_spec,domain_spec,domain_grid,sub_domain_origin,random_walk_spec)
        else:
            raise NameError('unknown random walk')
            
            
def Trajectory_Init(Param,bact,gradb_x,gradb_y,is_in_plate,trace_spec=(5,10),RandomWalkType='Langevin_Interaction_2',Dimensioned=True, After_inference=False,Data = None):
    """
    Wrapper that instanciates a list of swimmers
    
    Input:
        - Param :                   dictionnary of simulation parameters
        - bact [array]:             Temporal stack of bacterial density in biofilm
        - gradb_x [array]:          Temporal stack of biofilm density gradient (x direction)
        - gradb_y [array]:          Temporal stack of biofilm density gradient (y direction)
        - is_in_plate [array]:      Temporal stack of image mask : allows to avoid pixels outside the domain (for example outside the plate).
        - trace_spec :              Specification of the swimmer trace (to define the pore left by the swimmer)
        - randomwalkType :          Name of the random walk: only 'Langevin_Interaction_2' is coded
        - Dimensioned:                     switch between dimensioned or non-dimensioned models
        - After_inference [bool]    switch between different feature if initialization is before inference (initial position and velocity are random) or after inference (initial position and velocity are prescribed)
        - Data [data structure]     data with initial position and velocity if prescribed 
    Output:
        - swimmer_list_loc:     List of swimmer object
    """
    
    if After_inference:        
        swimmer_list_loc=list()
        random_walk_spec_eps=list()        
        for l in range(Data.swimmer_Nb):
            random_walk_spec_eps.append(np.random.multivariate_normal([0,0], np.eye(2),int(Param['Tf']/Param['dt'])))        
            swimmer = Brownian(Param['dt'],1,runtime=Param['Tf'], trace_spec=trace_spec, 
                               domain_spec=[Param['origin'], [(Param['xInit']+Param['Lx'])/Param['Xref'],(Param['yInit']+Param['Ly'])/Param['Xref']]],
                               domain_grid=[Param['m'],Param['n']],sub_domain_origin=Param['origin'],
                               random_walk_spec=(Param['inferred_swimmer_beta'],Param['inferred_swimmer_gamma'],
                               Param['inferred_swimmer_eps']*random_walk_spec_eps[l],Param['inferred_swimmer_v0_in_biofilm'],Param['inferred_swimmer_v1_in_matrix']),
                               RandomWalkType=RandomWalkType)
            #---- As initial condition for velocity and position are prescribed, velocity and positions are prescribed for time 0 and 1, and the trajectory starts at 1
            swimmer.trajectory.GetBiofilmColorCode(is_in_plate[1,:,:])
            swimmer.trajectory.GetBiofilm(bact[1,:,:],gradb_x[1,:,:],gradb_y[1,:,:])
            swimmer.trajectory.GetInitialPosition(
                np.array([
                    Data.swimmer_posX[l][0]/Param['Xref']+Data.swimmer_Vx[l][0]/Param['Vref']*Param['dt'] * Param['Vref']/Param['Xref'],
                    Data.swimmer_posY[l][0]/Param['Xref']+Data.swimmer_Vy[l][0]/Param['Vref']*Param['dt'] * Param['Vref']/Param['Xref']]),
                np.array([Data.swimmer_Vx[l][0]/Param['Vref'], Data.swimmer_Vy[l][0]/Param['Vref']]))
            swimmer.trajectory.time[1] = swimmer.trajectory.time[0]+swimmer.trajectory.dt
            swimmer.trajectory.swimmer_ID[1] = swimmer.trajectory.curr_swimmer_ID
            swimmer.trajectory.pos[1,:] = swimmer.trajectory.pos[0,:]
            swimmer.trajectory.vel[1,:] = swimmer.trajectory.vel[0,:]
            swimmer_list_loc.append(swimmer)    
    else:   
        #----Trajectories initialisation----#
        swimmer_list_loc=list()
        random_walk_spec_eps=list()
        for l in range(Param['swimmer_nb']):
            random_walk_spec_eps.append(np.random.multivariate_normal([0,0], np.eye(2),int(Param['Tf']/Param['dt'])))
            if Dimensioned:
                swimmer = Brownian(Param['dt'],1,runtime=Param['Tf'], trace_spec=trace_spec, domain_spec=[Param['origin'],[Param['xInit']+Param['Lx'],Param['yInit']+Param['Ly']]],
                    domain_grid=[Param['m'],Param['n']],sub_domain_origin=Param['origin'],
                    random_walk_spec=(Param['swimmer_beta'],Param['swimmer_gamma'],
                    Param['swimmer_eps']*random_walk_spec_eps[l],Param['swimmer_v0_in_biofilm'],Param['swimmer_v1_in_matrix']),
                    RandomWalkType=RandomWalkType)
            else:
                swimmer = Brownian(Param['dt'],1,runtime=Param['Tf'],trace_spec=trace_spec,
                    domain_spec=[Param['origin'],[Param['xInit']/Param['Xref']+Param['Lx']/Param['Xref'],Param['yInit']/Param['Xref']+Param['Ly']/Param['Xref']]], 
                    domain_grid=[Param['m'],Param['n']],sub_domain_origin=Param['origin'],
                    random_walk_spec=(Param['swimmer_beta'],Param['swimmer_gamma'],
                    Param['swimmer_eps']*random_walk_spec_eps[l],Param['swimmer_v0_in_biofilm'],Param['swimmer_v1_in_matrix']),
                    RandomWalkType=RandomWalkType)
            swimmer.trajectory.GetBiofilmColorCode(is_in_plate[0,:,:])
            swimmer.trajectory.GetBiofilm(bact[0,:,:],gradb_x[0,:,:],gradb_y[0,:,:])
            swimmer.trajectory.GetInitialPosition()
            swimmer_list_loc.append(swimmer)

    return swimmer_list_loc
    
    
